<?php

class Netenberg_Script_AbanteCart extends Netenberg_Script
{
    public function install($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $curl = new Netenberg_cURL;

        $step = 0;

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list(
            $parameters['mysql_hostname'],
            $parameters['mysql_username'],
            $parameters['mysql_password'],
            $parameters['mysql_database']
        ) = $control_panel->insertMysql();

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $operating_system->transpose(
            'http://jaist.dl.sourceforge.net/project/abantecart/abantecart_1.1.8.zip',
            array(
                'abantecart_1.1.8/public_html/*' => sprintf(
                    '%s/%s',
                    $parameters['document_root'],
                    $parameters['directory']
                ),
            )
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/admin/system',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/download',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/extensions',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/image',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/resources',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/system/cache',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/system/config.php',
                $parameters['document_root'],
                $parameters['directory']
            ), 666, false);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/system/logs',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?rt=license',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'agree' => '1',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?rt=settings',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?rt=install',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'db_driver' => 'mysql',
                'db_host' => $parameters['mysql_hostname'],
                'db_user' => $parameters['mysql_username'],
                'db_password' => $parameters['mysql_password'],
                'db_name' => $parameters['mysql_database'],
                'db_prefix' => '',
                'admin_path' => $parameters['admin_path'],
                'username' => $parameters['username'],
                'password' => $parameters['password'],
                'password_confirm' => $parameters['password'],
                'email' => $parameters['email'],
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?rt=install&runlevel=1',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?rt=install&runlevel=2',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?rt=install&runlevel=3',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?rt=install&runlevel=4',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?rt=install/progressbar&work=max',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?rt=install/progressbar&work=do',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?rt=activations&admin_path=%s',
                $parameters['domain'],
                $parameters['directory'],
                $parameters['admin_path']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list($output, $return_var) = $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?rt=finish',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );
        if (strpos($output[1], 'Installation Completed') !== false) {
            log_('DEBUG', sprintf(_('Step %d'), ++$step));
            $operating_system->rm(sprintf(
                '%s/%s/install',
                $parameters['document_root'],
                $parameters['directory']
            ), true);

            log_('DEBUG', 'Success');

            return parent::install($parameters);
        }
        log_('DEBUG', 'Failure');

        return false;
    }

    public function getCategory()
    {
        return _('E-Commerce');
    }

    public function getDescription()
    {
        return _('AbanteCart is a free shopping cart software or eCommerce solution to help merchants to set up online business, sell products or services on internet and easily manage website activity. AbanteCart (PHP based) application is built and supported by experienced enthusiasts that are passionate about their work and contribution to rapidly evolving eCommerce industry.');
    }

    public function getDetails($parameters)
    {
        $version_php = sprintf(
            '%s/%s/core/version.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($version_php)) {
            return false;
        }
        $contents = file_get_contents($version_php);
        preg_match(
            '#MASTER_VERSION\',\s*\'(\d+)\'#',
            $contents,
            $major_version
        );
        preg_match(
            '#MINOR_VERSION\',\s*\'(\d+)\'#',
            $contents,
            $minor_version
        );
        preg_match(
            '#VERSION_BUILT\',\s*\'(\d+)\'#',
            $contents,
            $version_built
        );

        return array(
            'version' => sprintf(
                '%s.%s.%s',
                $major_version[1],
                $minor_version[1],
                $version_built[1]
            ),
        );
    }

    public function getForm()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $form = new Netenberg_Form();
        $form->addElement('select', 'domain', array(
            'label' => _('Domain'),
            'multiOptions' => $control_panel->getDomains(),
            'required' => true,
        ));
        $form->addElement('text', 'directory', array(
            'description' => _('Leave this field empty if you want to install in the web root for the domain you\'ve selected (i.e., http://domain.com/ ). If you\'d like to install in a subdirectory, please enter the path to the directory relative to the web root for your domain. The final destination subdirectory should not exist, but all others can exist (e.g., http://domain.com/some/sub/directory - In this case, "directory" should not already exist).'),
            'filters' => array(new Netenberg_Filter_Directory()),
            'label' => _('Directory'),
            'validators' => array(new Netenberg_Validate_Directory()),
        ));
        $form->addElement('text', 'username', array(
            'description' => sprintf(_('maximum %d characters'), 20),
            'label' => _('Username'),
            'required' => true,
            'validators' => array(
                array('StringLength', false, array(
                    'max' => 20,
                )),
            )
        ));
        $form->addElement('text', 'password', array(
            'label' => _('Password'),
            'required' => true,
        ));
        $form->addElement('text', 'email', array(
            'label' => _('Email'),
            'required' => true,
            'validators' => array(
                array('EmailAddress', false),
            ),
        ));
        $form->addElement('text', 'admin_path', array(
            'label' => _('/admin/ Directory'),
            'required' => true,
            'validators' => array(
                array('StringLength', false, array(
                )),
            )
        ));
        $form->addElement('button', 'submit');
        $form->addElement('button', 'reset');
        $form->addDisplayGroup(
            array('domain', 'directory'),
            'location_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => 'Location Details',
            )
        );
        $form->addDisplayGroup(
            array(
                'username',
                'password',
                'email',
                'admin_path'
            ),
            'administrator_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => _('Administrator Details'),
            )
        );
        $form->addDisplayGroup(
            array('submit', 'reset'),
            'buttons',
            array(
                'decorators' => $form->getButtonGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
            )
        );

        return $form;
    }

    public function getImage()
    {
        return 'http://www.abantecart.com/templates/abantecart/images/shopping_cart_logo.png';
    }

    public function getName()
    {
        return 'AbanteCart';
    }

    public function getItems($parameters)
    {
        return array(
            _('Backend') => array(
                sprintf(
                    '<a href="http://%s/%s/index.php?s=%s" target="_blank">http://%s/%s/index.php?s=%s</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['admin_path'],
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['admin_path']
                ),
                sprintf(_('Username: %s'), $parameters['username']),
                sprintf(_('Password: %s'), $parameters['password']),
            ),
            _('Frontend') => array(
                sprintf(
                    '<a href="http://%s/%s" target="_blank">http://%s/%s</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['domain'],
                    $parameters['directory']
                ),
            ),
        );
    }

    public function getRequirements()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $apache = $control_panel->getApache();
        $mysql = $control_panel->getMysql();
        $php = $control_panel->getPhp();

        return array(
            'Disk Space' => (
                $control_panel->getSize() >= $this->getSize()
            )? true: false,
            'Apache 2+' => (
                strpos($apache, 'Apache/2') !== false
            )? true: false,
            'MySQL 5.0+' => (
                strpos($mysql, 'Distrib 5') !== false
            )? true: false,
            'PHP 5.2+' => (preg_match(
                '#PHP Version\s*=>\s*(5\.[2-9])#', $php
            ) === 1)? true: false,
            'PHP :: curl' => (preg_match(
                '#cURL support\s*=>\s*enabled#', $php
            ) === 1)? true: false,
        );
    }

    public function getSize()
    {
        return 50331648;
    }

    public function getSlug()
    {
        return 'abante-cart';
    }

    public function getTimestamp()
    {
        return '2014-05-14 00:00:00';
    }

    public function getUrls()
    {
        return array(
            _('Home') => 'http://www.abantecart.com',
            _('Community') => 'http://forum.abantecart.com',
            _('Documentation') => 'http://www.abantecart.com/ecommerce-documentation',
            _('Support') => 'http://www.abantecart.com/ecommerce-solution-support',
        );
    }

    public function getVersion()
    {
        return '1.1.8';
    }

    public function uninstall($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $config_php = sprintf(
            '%s/%s/system/config.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($config_php)) {
            return false;
        }
        $contents = file_get_contents($config_php);
        preg_match('#DB_DATABASE\', \s*\'([^\']*)#', $contents, $database);
        preg_match(
            '#DB_USERNAME\',\s*\'([^\']*)#', $contents, $mysql_username
        );
        $control_panel->deleteMysql($mysql_username[1], $database[1]);

        $operating_system->dispose(sprintf(
            '%s/%s', $parameters['document_root'], $parameters['directory']
        ));

        return parent::uninstall($parameters);
    }
}
