<?php

class Netenberg_Script_phpEnter extends Netenberg_Script
{
    public function install($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $curl = new Netenberg_cURL;

        $step = 0;

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list(
            $parameters['mysql_hostname'],
            $parameters['mysql_username'],
            $parameters['mysql_password'],
            $parameters['mysql_database']
        ) = $control_panel->insertMysql();

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $operating_system->transpose(
            'http://softlayer-sng.dl.sourceforge.net/project/phpenter/www427.zip',
            array(
                '*' => sprintf(
                    '%s/%s',
                    $parameters['document_root'],
                    $parameters['directory']
                ),
            )
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/admin',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/classes',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/install',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/temp',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/uploads',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/salt.php',
                $parameters['document_root'],
                $parameters['directory']
            ), 666, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $parse_url = parse_url(sprintf(
            'http://%s/%s', $parameters['domain'], $parameters['directory']
        ));
        $htaccess = sprintf(
            '%s/%s/.htaccess',
            $parameters['document_root'],
            $parameters['directory']
        );
        $contents = file_get_contents($htaccess);
        $contents = preg_replace(
            '#RewriteEngine [oO]n#',
            sprintf("RewriteEngine on\nRewriteBase %s", $parse_url['path']),
            $contents
        );
        file_put_contents($htaccess, $contents);

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/index.php',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/install1.php',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'host' => $parameters['mysql_hostname'],
                'name' => $parameters['mysql_database'],
                'pass' => $parameters['mysql_password'],
                'submit' => 'Next Step',
                'user' => $parameters['mysql_username'],
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/install2.php',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/install3.php',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'incpath http' =>
                sprintf('%s/%s',
                    $parameters['domain'],
                    $parameters['directory']
                ),
                'submit' => 'Next Step',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list($output, $return_var) = $curl->request(
            sprintf(
                'http://%s/%s/install/install4.php',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'email' => $parameters['admin_mail'],
                'password' => $parameters['admin_pass'],
                'submit' => 'Admin Account',
                'username' => $parameters['admin_login'],
            ),
            array(),
            array()
        );
        if (
            strpos($output[1], 'Installation Completed Successfully') !== false
        ) {
            log_('DEBUG', sprintf(_('Step %d'), ++$step));
            $operating_system->rm(sprintf(
                '%s/%s/install',
                $parameters['document_root'],
                $parameters['directory']
            ), true);

            log_('DEBUG', 'Success');

            return parent::install($parameters);
        }
        log_('DEBUG', 'Failure');

        return false;
    }


    public function getCategory()
    {
        return _('Portals/CMS');
    }

    public function getDescription()
    {
        return _('An online news publishing system that features easy installation, user submission, and an admin panel for adding, editing, and removing categories and news.');
    }

    public function getDetails($parameters)
    {
        $header_php = sprintf(
            '%s/%s/admin/header.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($header_php)) {
            return false;
        }
        $contents = file_get_contents($header_php);
        preg_match('#Version:\s*(.*)\.\<#', $contents, $version);

        return array(
            'version' => $version[1],
        );
    }

    public function getForm()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $form = new Netenberg_Form();
        $form->addElement('select', 'domain', array(
            'label' => _('Domain'),
            'multiOptions' => $control_panel->getDomains(),
            'required' => true,
        ));
        $form->addElement('text', 'directory', array(
            'description' => _('Leave this field empty if you want to install in the web root for the domain you\'ve selected (i.e., http://domain.com/ ). If you\'d like to install in a subdirectory, please enter the path to the directory relative to the web root for your domain. The final destination subdirectory should not exist, but all others can exist (e.g., http://domain.com/some/sub/directory - In this case, "directory" should not already exist).'),
            'filters' => array(new Netenberg_Filter_Directory()),
            'label' => _('Directory'),
            'validators' => array(new Netenberg_Validate_Directory()),
        ));
        $form->addElement('text', 'admin_login', array(
            'label' => _('Username'),
            'required' => true,
        ));
        $form->addElement('text', 'admin_pass', array(
            'label' => _('Password'),
            'required' => true,
        ));
        $form->addElement('text', 'admin_mail', array(
            'label' => _('Email'),
            'required' => true,
            'validators' => array(
                array('EmailAddress', false),
            ),
        ));
        $form->addElement('button', 'submit');
        $form->addElement('button', 'reset');
        $form->addDisplayGroup(
            array('domain', 'directory'),
            'location_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => _('Location Details'),
            )
        );
        $form->addDisplayGroup(
            array('admin_login', 'admin_pass', 'admin_mail',),
            'administrator_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => _('Administrator Details'),
            )
        );
        $form->addDisplayGroup(
            array('submit', 'reset'),
            'buttons',
            array(
                'decorators' => $form->getButtonGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
            )
        );

        return $form;
    }

    public function getImage()
    {
        return '';
    }

    public function getName()
    {
        return 'phpEnter';
    }

    public function getItems($parameters)
    {
        return array(
            _('Backend') => array(
                sprintf(
                    '<a href="http://%s/%s/admin" target="_blank">http://%s/%s/admin</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['domain'],
                    $parameters['directory']
                ),
                sprintf(_('Username: %s'), $parameters['admin_login']),
                sprintf(_('Password: %s'), $parameters['admin_pass']),
            ),
            _('Frontend') => array(
                sprintf(
                    '<a href="http://%s/%s" target="_blank">http://%s/%s</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['domain'],
                    $parameters['directory']
                ),
            ),
        );
    }

    public function getRequirements()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $apache = $control_panel->getApache();
        $mysql = $control_panel->getMysql();
        $php = $control_panel->getPhp();

        return array(
            'Disk Space' => (
                $control_panel->getSize() >= $this->getSize()
            )? true: false,
            'Apache 2+' => (
                strpos($apache, 'Apache/2') !== false
            )? true: false,
            'Apache :: mod_rewrite' => true,
            'MySQL 5.0+' => (
                strpos($mysql, 'Distrib 5') !== false
            )? true: false,
            'PHP 5.0+' => (preg_match(
                '#PHP Version\s*=>\s*(5\.[0-9])#', $php
            ) === 1)? true: false,
            'PHP :: gd' => (preg_match(
                '#GD Support\s*=>\s*enabled#', $php
            ) === 1)? true: false,
        );
    }

    public function getSize()
    {
        return 5452595;
    }

    public function getSlug()
    {
        return 'php-enter';
    }

    public function getTimestamp()
    {
        return '2014-04-16 15:18:58';
    }

    public function getUrls()
    {
        return array(
            _('Home') => 'http://www.phpenter.net',
            _('Support') => 'http://forums.phpenter.net',
        );
    }

    public function getVersion()
    {
        return '4.2.7';
    }

    public function uninstall($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $config_php = sprintf(
            '%s/%s/admin/config.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($config_php)) {
            return false;
        }
        $contents = file_get_contents($config_php);
        preg_match('#database\s*=\s*\'([^\']*)#', $contents, $database);
        preg_match(
            '#user\s*=\s*\'([^\']*)#', $contents, $mysql_username
        );
        $control_panel->deleteMysql($mysql_username[1], $database[1]);

        $operating_system->dispose(sprintf(
            '%s/%s', $parameters['document_root'], $parameters['directory']
        ));

        return parent::uninstall($parameters);
    }
}
