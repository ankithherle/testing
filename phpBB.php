<?php

class Netenberg_Script_phpBB extends Netenberg_Script
{
    public function install($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $curl = new Netenberg_cURL;

        $step = 0;

        $parse_url = parse_url(sprintf(
            'http://%s/phpbb', $parameters['domain']
        ));

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list(
            $parameters['mysql_hostname'],
            $parameters['mysql_username'],
            $parameters['mysql_password'],
            $parameters['mysql_database']
        ) = $control_panel->insertMysql();

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $operating_system->transpose(
            'https://www.phpbb.com/files/release/phpBB-3.1.2.zip',
            array(
                'phpBB3/*' => sprintf(
                    '%s/%s',
                    $parameters['document_root'],
                    $parameters['directory']
                ),
            )
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/cache',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/common.php',
                $parameters['document_root'],
                $parameters['directory']
            ), 666, false);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/config.php',
                $parameters['document_root'],
                $parameters['directory']
            ), 666, false);
        }

        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/files',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/language/en/common.php',
                $parameters['document_root'],
                $parameters['directory']
            ), 666, false);
        }

        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/store',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/index.php',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'language' => 'en',
                'mode' => 'install',
                'sub' => 'requirements',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?mode=install&sub=database&language=en',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'img_imagick' => '/usr/bin/',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?mode=install&sub=database',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'dbhost' => $parameters['mysql_hostname'],
                'dbms' => 'mysql',
                'dbname' => $parameters['mysql_database'],
                'dbpasswd' => $parameters['mysql_password'],
                'dbport' => '',
                'dbuser' => $parameters['mysql_username'],
                'img_imagick' => '/usr/bin/',
                'language' => 'en',
                'table_prefix' => 'phpbb_',
                'testdb' => 'true',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?mode=install&sub=administrator',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'dbhost' => $parameters['mysql_hostname'],
                'dbms' => 'mysqli',
                'dbname' => $parameters['mysql_database'],
                'dbpasswd' => $parameters['mysql_password'],
                'dbport' => '',
                'dbuser' => $parameters['mysql_username'],
                'img_imagick' => '/usr/bin/',
                'language' => 'en',
                'table_prefix' => 'phpbb_',
                'testdb' => 'true',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?mode=install&sub=administrator',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'admin_name' => $parameters['admin_username'],
                'admin_pass1' => $parameters['admin_password'],
                'admin_pass2' => $parameters['admin_password'],
                'board_email1' => $parameters['admin_email'],
                'board_email2' => $parameters['admin_email'],
                'check' => 'true',
                'dbhost' => $parameters['mysql_hostname'],
                'dbms' => 'mysqli',
                'dbname' => $parameters['mysql_database'],
                'dbpasswd' => $parameters['mysql_password'],
                'dbport' => '',
                'dbuser' => $parameters['mysql_username'],
                'default_lang' => 'en',
                'img_imagick' => '/usr/bin/',
                'img_imagick' => '/usr/bin/',
                'language' => 'en',
                'table_prefix' => 'phpbb_',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?mode=install&sub=config_file',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'admin_name' => $parameters['admin_username'],
                'admin_pass1' => $parameters['admin_password'],
                'admin_pass2' => $parameters['admin_password'],
                'board_email1' => $parameters['admin_email'],
                'board_email2' => $parameters['admin_email'],
                'dbhost' => $parameters['mysql_hostname'],
                'dbms' => 'mysqli',
                'dbname' => $parameters['mysql_database'],
                'dbpasswd' => $parameters['mysql_password'],
                'dbport' => '',
                'dbuser' => $parameters['mysql_username'],
                'default_lang' => 'en',
                'img_imagick' => '/usr/bin/',
                'img_imagick' => '/usr/bin/',
                'language' => 'en',
                'table_prefix' => 'phpbb_',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?mode=install&sub=advanced',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'admin_name' => $parameters['admin_username'],
                'admin_pass1' => $parameters['admin_password'],
                'admin_pass2' => $parameters['admin_password'],
                'board_email1' => $parameters['admin_email'],
                'board_email2' => $parameters['admin_email'],
                'dbhost' => $parameters['mysql_hostname'],
                'dbms' => 'mysqli',
                'dbname' => $parameters['mysql_database'],
                'dbpasswd' => $parameters['mysql_password'],
                'dbport' => '',
                'dbuser' => $parameters['mysql_username'],
                'default_lang' => 'en',
                'img_imagick' => '/usr/bin/',
                'img_imagick' => '/usr/bin/',
                'language' => 'en',
                'table_prefix' => 'phpbb_',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?mode=install&sub=create_table',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'admin_name' => $parameters['admin_username'],
                'admin_pass1' => $parameters['admin_password'],
                'admin_pass2' => $parameters['admin_password'],
                'board_email1' => $parameters['admin_email'],
                'board_email2' => $parameters['admin_email'],
                'cookie_secure' => '0',
                'dbhost' => $parameters['mysql_hostname'],
                'dbms' => 'mysqli',
                'dbname' => $parameters['mysql_database'],
                'dbpasswd' => $parameters['mysql_password'],
                'dbport' => '',
                'dbuser'  => $parameters['mysql_username'],
                'default_lang' => 'en',
                'email_enable' => '1',
                'force_server_vars' => '0',
                'img_imagick' => '/usr/bin/',
                'language' => 'en',
                'script_path' => $parse_url['path'],
                'server_name' => $parse_url['host'],
                'server_port' => '80',
                'server_protocol' => 'http://',
                'smtp_auth' => 'PLAIN',
                'smtp_delivery' => '0',
                'smtp_host' => '',
                'smtp_pass' => '',
                'smtp_user' => '',
                'table_prefix' => 'phpbb_',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list($output, $return_var) = $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?mode=install&sub=final',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'admin_name' => $parameters['admin_username'],
                'admin_pass1' => $parameters['admin_password'],
                'admin_pass2' => $parameters['admin_password'],
                'board_email1' => $parameters['admin_email'],
                'board_email2' => $parameters['admin_email'],
                'cookie_secure' => '0',
                'dbhost' => $parameters['mysql_hostname'],
                'dbms' => 'mysqli',
                'dbname' => $parameters['mysql_database'],
                'dbpasswd' => $parameters['mysql_password'],
                'dbport' => '',
                'dbuser' => $parameters['mysql_username'],
                'default_lang' => 'en',
                'email_enable' => '1',
                'force_server_vars' => '0',
                'ftp_pass' => '',
                'ftp_path' => '',
                'ftp_user' => '',
                'img_imagick' => '/usr/bin/',
                'language' => 'en',
                'script_path' => $parse_url['path'],
                'server_name' => $parse_url['host'],
                'server_port' => '80',
                'server_protocol' => 'http://',
                'smtp_auth' => 'PLAIN',
                'smtp_delivery' => '0',
                'smtp_host' => '',
                'smtp_pass' => '',
                'smtp_user'  => '',
                'table_prefix' => 'phpbb_',
            ),
            array(),
            array()
        );
        if (strpos(
            $output[1], 'You have successfully installed'
        ) !== false) {
            log_('DEBUG', sprintf(_('Step %d'), ++$step));
            $operating_system->rm(sprintf(
                '%s/%s/install',
                $parameters['document_root'],
                $parameters['directory']
            ), true);

            log_('DEBUG', 'Success');

            return parent::install($parameters);
        }
        log_('DEBUG', 'Failure');

        return false;
    }

    public function getCategory()
    {
        return _('Forums');
    }

    public function getDescription()
    {
        return _('phpBB is a free flat-forum bulletin board software solution that can be used to stay in touch with a group of people or can power your entire website. With an extensive database of user-created modifications and styles database containing hundreds of style and image packages to customise your board, you can create a very unique forum in minutes.');
    }

    public function getDetails($parameters)
    {
        $constants_php = sprintf(
            '%s/%s/includes/constants.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($constants_php)) {
            return false;
        }
        $contents = file_get_contents($constants_php);
        preg_match(
            '#define\(\'PHPBB_VERSION\',\s*\'([^\']*)#', $contents, $version
        );

        return array(
            'version' => $version[1],
        );
    }

    public function getForm()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $form = new Netenberg_Form();
        $form->addElement('select', 'domain', array(
            'label' => _('Domain'),
            'multiOptions' => $control_panel->getDomains(),
            'required' => true,
        ));
        $form->addElement('text', 'directory', array(
            'description' => _('Leave this field empty if you want to install in the web root for the domain you\'ve selected (i.e., http://domain.com/ ). If you\'d like to install in a subdirectory, please enter the path to the directory relative to the web root for your domain. The final destination subdirectory should not exist, but all others can exist (e.g., http://domain.com/some/sub/directory - In this case, "directory" should not already exist).'),
            'filters' => array(new Netenberg_Filter_Directory()),
            'label' => _('Directory'),
            'validators' => array(new Netenberg_Validate_Directory()),
        ));
        $form->addElement('text', 'admin_username', array(
            'description' => sprintf(_('minimum %d characters'), 3),
            'label' => _('Username'),
            'required' => true,
            'validators' => array(
                array('StringLength', false, array(
                    'min' => 3,
                )),
            ),
        ));
        $form->addElement('text', 'admin_password', array(
            'description' => sprintf(_('minimum %d characters'), 6),
            'label' => _('Password'),
            'required' => true,
            'validators' => array(
                array('StringLength', false, array(
                    'min' => 6,
                )),
            ),
        ));
        $form->addElement('text', 'admin_email', array(
            'label' => _('Email'),
            'required' => true,
            'validators' => array(
                array('EmailAddress', false),
            ),
        ));
        $form->addElement('button', 'submit');
        $form->addElement('button', 'reset');
        $form->addDisplayGroup(
            array('domain', 'directory'),
            'location_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => 'Location Details',
            )
        );
        $form->addDisplayGroup(
            array(
                'admin_username',
                'admin_password',
                'admin_email',
            ),
            'administrator_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => _('Administrator Details'),
            )
        );
        $form->addDisplayGroup(
            array('submit', 'reset'),
            'buttons',
            array(
                'decorators' => $form->getButtonGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
            )
        );

        return $form;
    }

    public function getImage()
    {
        return 'https://www.phpbb.com/theme/images/logo_phpbb.png';
    }

    public function getName()
    {
        return 'phpBB';
    }

    public function getItems($parameters)
    {
        return array(
            _('Backend') => array(
                sprintf(
                    '<a href="http://%s/%s/" target="_blank">http://%s/%s/</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['domain'],
                    $parameters['directory']
                ),
                sprintf(_('Username: %s'), $parameters['admin_username']),
                sprintf(_('Password: %s'), $parameters['admin_password']),
            ),
        );
    }

    public function getRequirements()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $apache = $control_panel->getApache();
        $mysql = $control_panel->getMysql();
        $php = $control_panel->getPhp();

        return array(
            'Disk Space' => (
                $control_panel->getSize() >= $this->getSize()
            )? true: false,
            'Apache 1+' => (
                strpos($apache, 'Apache/1') !== false
                or
                strpos($apache, 'Apache/2') !== false
            )? true: false,
            'MySQL 4.0+' => (
                strpos($mysql, 'Distrib 4') !== false
                or
                strpos($mysql, 'Distrib 5') !== false
            )? true: false,
            'PHP 4.3.3+' => (preg_match(
                '#PHP Version\s*=>\s*(4\.3\.[3-9]|4\.3\.1[0-9]|4\.4\.[0-9]|5)#',
                $php
            ) === 1)? true: false,
            'PHP :: gd' => (preg_match(
                '#GD\s*Support\s*=>\s*enabled#', $php
            ) === 1)? true: false,
            'PHP :: ftp' => (preg_match(
                '#FTP\s*support\s*=>\s*enabled#', $php
            ) === 1)? true: false,
            'PHP :: zlib' => (preg_match(
                '#ZLib\s*Support\s*=>\s*enabled#', $php
            ) === 1)? true: false,
            'PHP :: xml' => (preg_match(
                '#XML\s*Support\s*=>\s*active#', $php
            ) === 1)? true: false,
            'PHP :: ImageMagick' => true,
        );
    }

    public function getSize()
    {
        return 13107200;
    }

    public function getSlug()
    {
        return 'phpbb';
    }

    public function getTimestamp()
    {
        return '2014-11-25 00:00:00';
    }

    public function getUrls()
    {
        return array(
            _('Home') => 'https://www.phpbb.com',
            _('Community') => 'https://www.phpbb.com/community',
            _('Documentation') => 'https://www.phpbb.com/support/documentation/3.0/',
            _('Support') => 'https://www.phpbb.com/support/forums/',
        );
    }

    public function getVersion()
    {
        return '3.1.2';
    }

    public function uninstall($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $config_php = sprintf(
            '%s/%s/config.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($config_php)) {
            return false;
        }
        $contents = file_get_contents($config_php);
        preg_match('#dbuser\s*=\s*\'([^\']*)#', $contents, $mysql_username);
        preg_match('#dbname\s*=\s*\'([^\']*)#', $contents, $database);
        $control_panel->deleteMysql($mysql_username[1], $database[1]);

        $operating_system->dispose(sprintf(
            '%s/%s', $parameters['document_root'], $parameters['directory']
        ));

        return parent::uninstall($parameters);
    }
}
