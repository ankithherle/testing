<?php

class Netenberg_Script_OpenSourceSocialNetwork extends Netenberg_Script
{
    public function install($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $curl = new Netenberg_cURL;

        $step = 0;

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list(
            $parameters['mysql_hostname'],
            $parameters['mysql_username'],
            $parameters['mysql_password'],
            $parameters['mysql_database']
        ) = $control_panel->insertMysql();

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $operating_system->transpose(
            'http://www.opensource-socialnetwork.org/downloads/ossn-v3.5-1443211017.zip',
            array(
                'ossn/*' => sprintf(
                    '%s/%s',
                    $parameters['document_root'],
                    $parameters['directory']
                ),
            )
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $operating_system->mkdir(sprintf(
            '%s/%s', $control_panel->getHome(), $parameters['directory']
        ), 777);

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/configurations',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/installation/?action=install',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'datadir' => sprintf(
                    '%s/%s/',
                    $control_panel->getHome(),
                    $parameters['directory']
                ),
                'dbhost' => $parameters['mysql_hostname'],
                'dbname' => $parameters['mysql_database'],
                'dbpwd' => $parameters['mysql_password'],
                'dbuser' => $parameters['mysql_username'],
                'notification_email' => $parameters['email'],
                'owner_email' => $parameters['email'],
                'sitename' => $parameters['site_name'],
                'url' => sprintf(
                    'http://%s/%s/',
                    $parameters['domain'],
                    $parameters['directory']
                ),
            ),
            array(),
            array()
        );
        $date = explode('/', $parameters['date_of_birth']);

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/installation/?action=account',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'birthday' => $date[1],
                'birthm' => $date[0],
                'birthy' => $date[2],
                'email' => $parameters['email'],
                'email_re' => $parameters['email'],
                'firstname' => $parameters['firstname'],
                'gender' => $parameters['gender'],
                'lastname' => $parameters['lastname'],
                'password' => $parameters['password'],
                'username' => $parameters['username'],
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list($output, $return_var) = $curl->request(
            sprintf(
                'http://%s/%s/installation?page=installed',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );
        if (strpos(
            $output[1], 'Open Source Social Network has been installed'
        ) !== false) {
            log_('DEBUG', sprintf(_('Step %d'), ++$step));
            $operating_system->rm(sprintf(
                '%s/%s/installation',
                $parameters['document_root'],
                $parameters['directory']
            ), true);

            log_('DEBUG', 'Success');

            return parent::install($parameters);
        }
        log_('DEBUG', 'Failure');

        return false;
    }

    public function getCallback($contents)
    {
        if (!(preg_match('#\d{2}\/\d{2}\/\d{4}#', $contents))) {
            return false;
        }
        $date = explode('/', $contents);
        if (!checkdate(
            intval($date[0]), intval($date[1]), intval($date[2])
        )) {
            return false;
        }
        $now = time();
        if (strtotime($contents) >= $now) {
            return false;
        }

        return true;
    }

    public function getCategory()
    {
        return _('Social Networking');
    }

    public function getDescription()
    {
        return _('Opensource-Socialnetwork also know as OSSN is a social networking software. It allows you to make a social networking website, helps your members build social relationships with people who share similar professional or personal interests.');
    }

   public function getDetails($parameters)
    {
        $opensource_socialnetwork_xml = sprintf(
            '%s/%s/opensource-socialnetwork.xml',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($opensource_socialnetwork_xml)) {
            return false;
        }
        $contents = file_get_contents($opensource_socialnetwork_xml);
        preg_match('#version\>([^<]*)#', $contents, $version);

        return array(
            'version' => $version[1],
        );
    }

    public function getForm()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $form = new Netenberg_Form();
        $form->addElement('select', 'domain', array(
            'label' => _('Domain'),
            'multiOptions' => $control_panel->getDomains(),
            'required' => true,
        ));
        $form->addElement('text', 'directory', array(
            'description' => _('Leave this field empty if you want to install in the web root for the domain you\'ve selected (i.e., http://domain.com/ ). If you\'d like to install in a subdirectory, please enter the path to the directory relative to the web root for your domain. The final destination subdirectory should not exist, but all others can exist (e.g., http://domain.com/some/sub/directory - In this case, "directory" should not already exist).'),
            'filters' => array(new Netenberg_Filter_Directory()),
            'label' => _('Directory'),
            'validators' => array(new Netenberg_Validate_Directory()),
        ));
        $form->addElement('text', 'username', array(
            'label' => _('Username'),
            'required' => true,
        ));
        $form->addElement('text', 'password', array(
            'label' => _('Password'),
            'required' => true,
        ));
        $form->addElement('text', 'email', array(
            'label' => _('Email'),
            'required' => true,
            'validators' => array(
                array('EmailAddress', false),
            ),
        ));
        $form->addElement('text', 'firstname', array(
            'label' => _('First Name'),
            'required' => true,
        ));
        $form->addElement('text', 'lastname', array(
            'label' => _('Last Name'),
            'required' => true,
        ));
        $form->addElement('select', 'gender', array(
            'label' => _('Gender'),
            'multiOptions' => array('Male'=> 'Male', 'Female' => 'Female'),
            'required' => true,
        ));
        $form->addElement('text', 'date_of_birth', array(
            'description' => sprintf(_('Format for the Date of Birth should be MM/dd/yyyy')),
            'label' => _('Date of Birth'),
            'required' => true,
            'validators' => array(
                array('Callback', false, array(
                    'callback' => array($this, 'getCallback'),
                )),
            )
        ));
        $form->addElement('text', 'site_name', array(
            'label' => _('Site Name'),
            'required' => true,
        ));
        $form->addElement('button', 'submit');
        $form->addElement('button', 'reset');
        $form->addDisplayGroup(
            array('domain', 'directory'),
            'location_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => 'Location Details',
            )
        );
        $form->addDisplayGroup(
            array(
                'username',
                'password',
                'email',
                'firstname',
                'lastname',
                'date_of_birth',
                'gender',
            ),
            'administrator_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => _('Administrator Details'),
            )
        );
        $form->addDisplayGroup(
            array(
                'site_name',
            ),
            'other_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => _('Other Details'),
            )
        );
        $form->addDisplayGroup(
            array('submit', 'reset'),
            'buttons',
            array(
                'decorators' => $form->getButtonGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
            )
        );

        return $form;
    }

    public function getImage()
    {
        return 'https://www.opensource-socialnetwork.org/templates/informatikon/media/logo.png';
    }

    public function getName()
    {
        return 'Open Source Social Network';
    }

    public function getItems($parameters)
    {
        return array(
            _('Backend') => array(
                sprintf(
                    '<a href="http://%s/%s/administrator" target="_blank">http://%s/%s/administrator</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['domain'],
                    $parameters['directory']
                ),
                sprintf(_('Username: %s'), $parameters['username']),
                sprintf(_('Password: %s'), $parameters['password']),
            ),
            _('Frontend') => array(
                sprintf(
                    '<a href="http://%s/%s" target="_blank">http://%s/%s</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['domain'],
                    $parameters['directory']
                ),
            ),
        );
    }

    public function getRequirements()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $apache = $control_panel->getApache();
        $mysql = $control_panel->getMysql();
        $php = $control_panel->getPhp();

        return array(
            'Disk Space' => (
                $control_panel->getSize() >= $this->getSize()
            )? true: false,
            'Apache 1+' => (
                strpos($apache, 'Apache/1') !== false
                or
                strpos($apache, 'Apache/2') !== false
            )? true: false,
            'Apache :: mod_rewrite' => true,
            'MySQL 5.0+/MariaDB 10.0+' => (preg_match(
                '#(Distrib\s*10\.[0-9])(?=.*?MariaDB)|(Distrib\s*(5\.[0-9]))#',
                $mysql
            ) === 1)? true: false,
            'PHP 5.3+' => (preg_match(
                '#PHP Version\s*=>\s*(5\.3\.[0-9]|5\.[4-9]|5\.3\.1[0-9]||5\.3\.2[0-9]|)#', $php
            ) === 1)? true: false,
            'PHP :: curl' => (preg_match(
                '#cURL support\s*=>\s*enabled#', $php
            ) === 1)? true: false,
            'PHP :: gd' => (preg_match(
                '#GD Support\s*=>\s*enabled#', $php
            ) === 1)? true: false,
            'PHP :: json' => (preg_match(
                '#json\s*support\s*=>\s*enabled#', $php
            ) === 1)? true: false,
            'PHP :: xml' => (preg_match(
                '#XML Support\s*=>\s*active#', $php
            ) === 1)? true: false,
        );
    }

    public function getSize()
    {
        return 8965446;
    }

    public function getSlug()
    {
        return 'open-source-social-network';
    }

    public function getTimestamp()
    {
        return '2015-09-25 00:00:00';
    }

    public function getUrls()
    {
        return array(
            _('Home') => 'https://www.opensource-socialnetwork.org',
            _('Community') => 'http://community.opensource-socialnetwork.org',
            _('Documentation') => 'http://docs.opensource-socialnetwork.org',
        );
    }

    public function getVersion()
    {
        return '3.5';
    }

    public function uninstall($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $ossn_config_db_php = sprintf(
            '%s/%s/configurations/ossn.config.db.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($ossn_config_db_php)) {
            return false;
        }
        $contents = file_get_contents($ossn_config_db_php);
        preg_match('#database\s*=\s*\'([^\']*)#', $contents, $database);
        preg_match('#user\s*=\s*\'([^\']*)#', $contents, $mysql_username);
        $control_panel->deleteMysql($mysql_username[1], $database[1]);

        $operating_system->dispose(sprintf(
            '%s/%s', $parameters['document_root'], $parameters['directory']
        ));

        return parent::uninstall($parameters);
    }
}
