<?php

class Netenberg_Script_AardvarkTopsitesPHP extends Netenberg_Script
{
    public function install($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $curl = new Netenberg_cURL;

        $step = 0;

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list(
            $parameters['mysql_hostname'],
            $parameters['mysql_username'],
            $parameters['mysql_password'],
            $parameters['mysql_database']
        ) = $control_panel->insertMysql();

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $operating_system->transpose(
            'http://www.aardvarktopsitesphp.com/atsphp-5.2.1.zip',
            array(
                'atsphp-5.2.1/*' => sprintf(
                    '%s/%s',
                    $parameters['document_root'],
                    $parameters['directory']
                ),
            )
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/settings_sql.php',
                $parameters['document_root'],
                $parameters['directory']
            ), 666, false);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list($output, $return_var) = $curl->request(
            sprintf(
                'http://%s/%s/install/index.php?l=english',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'admin_password' => $parameters['password'],
                'l' => 'english',
                'list_url' => sprintf(
                    'http://%s/%s',
                    $parameters['domain'],
                    $parameters['directory']
                ),
                'sql' => 'mysql',
                'sql_database' => $parameters['mysql_database'],
                'sql_host' => $parameters['mysql_hostname'],
                'sql_password' => $parameters['mysql_password'],
                'sql_prefix' => 'ats_',
                'sql_username' => $parameters['mysql_username'],
                'submit' => 'Install',
                'your_email' => $parameters['email'],
            ),
            array(),
            array()
        );
        if (strpos(
            $output[1], 'Your topsites list has been installed'
        ) !== false) {
            log_('DEBUG', sprintf(_('Step %d'), ++$step));
            $operating_system->rm(sprintf(
                '%s/%s/install',
                $parameters['document_root'],
                $parameters['directory']
            ), true);

            log_('DEBUG', 'Success');

            return parent::install($parameters);
        }
        log_('DEBUG', 'Failure');

        return false;
    }

    public function getCategory()
    {
        return _('Polls and Surveys');
    }

    public function getDescription()
    {
        return _('A topsites list ranks a group of related sites by popularity. Webmasters join the topsites list and are given a button to put on their site and link back to the topsites list. Aardvark Topsites PHP is a free topsites script built on PHP and MySQL. It is licensed under the GNU General Public License so it will always remain completely free. Its functionality compares favorably with other free and commercial scripts.');
    }

    public function getDetails($parameters)
    {
        $index_php = sprintf(
            '%s/%s/index.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($index_php)) {
            return false;
        }
        $contents = file_get_contents($index_php);
        preg_match(
            '#TMPL\[\'version\'\]\s*=\s*\'([\d+\.]+)\'#', $contents, $version
        );

        return array(
            'version' => $version[1],
        );
    }

    public function getForm()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $form = new Netenberg_Form();
        $form->addElement('select', 'domain', array(
            'label' => _('Domain'),
            'multiOptions' => $control_panel->getDomains(),
            'required' => true,
        ));
        $form->addElement('text', 'directory', array(
            'description' => _('Leave this field empty if you want to install in the web root for the domain you\'ve selected (i.e., http://domain.com/ ). If you\'d like to install in a subdirectory, please enter the path to the directory relative to the web root for your domain. The final destination subdirectory should not exist, but all others can exist (e.g., http://domain.com/some/sub/directory - In this case, "directory" should not already exist).'),
            'filters' => array(new Netenberg_Filter_Directory()),
            'label' => _('Directory'),
            'validators' => array(new Netenberg_Validate_Directory()),
        ));
        $form->addElement('text', 'email', array(
            'label' => _('Email'),
            'required' => true,
            'validators' => array(
                array('EmailAddress', false),
            ),
        ));
        $form->addElement('text', 'password', array(
            'label' => _('Password'),
            'required' => true,
        ));
        $form->addElement('button', 'submit');
        $form->addElement('button', 'reset');
        $form->addDisplayGroup(
            array('domain', 'directory'),
            'location_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => 'Location Details',
            )
        );
        $form->addDisplayGroup(
            array('email', 'password'),
            'administrator_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => _('Administrator Details'),
            )
        );
        $form->addDisplayGroup(
            array('submit', 'reset'),
            'buttons',
            array(
                'decorators' => $form->getButtonGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
            )
        );

        return $form;
    }

    public function getImage()
    {
        return 'http://www.aardvarktopsitesphp.com/images/header.png';
    }

    public function getName()
    {
        return 'Aardvark Topsites PHP';
    }

    public function getItems($parameters)
    {
        return array(
            _('Backend') => array(
                sprintf(
                    '<a href="http://%s/%s/?a=admin" target="_blank">http://%s/%s/?a=admin</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['domain'],
                    $parameters['directory']
                ),
                sprintf(_('Email: %s'), $parameters['email']),
                sprintf(_('Password: %s'), $parameters['password']),
            ),
            _('Frontend') => array(
                sprintf(
                    '<a href="http://%s/%s" target="_blank">http://%s/%s</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['domain'],
                    $parameters['directory']
                ),
            ),
        );
    }

    public function getRequirements()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $apache = $control_panel->getApache();
        $mysql = $control_panel->getMysql();
        $php = $control_panel->getPhp();

        return array(
            'Disk Space' => (
                $control_panel->getSize() >= $this->getSize()
            )? true: false,
            'Apache 1+' => (
                strpos($apache, 'Apache/1') !== false
                or
                strpos($apache, 'Apache/2') !== false
            )? true: false,
            'MySQL 4.0.4+' => (
                strpos($mysql, 'Distrib 4') !== false
                or
                strpos($mysql, 'Distrib 5') !== false
            )? true: false,
            'PHP 4.3+' => (preg_match(
                '#PHP Version\s*=>\s*(4\.3|4\.4|5)#', $php
            ) === 1)? true: false,
            'PHP :: gd' => (preg_match(
                '#GD Support\s*=>\s*enabled#', $php
            ) === 1)? true: false,
        );
    }

    public function getSize()
    {
        return 1572864;
    }

    public function getSlug()
    {
        return 'aardvark-topsites-php';
    }

    public function getTimestamp()
    {
        return '2009-03-21 00:00:00';
    }

    public function getUrls()
    {
        return array(
            _('Home') => 'http://www.aardvarktopsitesphp.com',
            _('Support') => 'http://www.aardvarktopsitesphp.com/forums',
        );
    }

    public function getVersion()
    {
        return '5.2.1';
    }

    public function uninstall($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $settings_sql_php = sprintf(
            '%s/%s/settings_sql.php',
             $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($settings_sql_php)) {
            return false;
        }
        $contents = file_get_contents($settings_sql_php);
        preg_match(
            '#sql_database\'\]\s*=\s*\'([^\']*)#', $contents, $database
        );
        preg_match(
            '#sql_username\'\]\s*=\s*\'([^\']*)#', $contents, $mysql_username
        );
        $control_panel->deleteMysql($mysql_username[1], $database[1]);

        $operating_system->dispose(sprintf(
            '%s/%s', $parameters['document_root'], $parameters['directory']
        ));

        return parent::uninstall($parameters);
    }
}
