<?php

class Netenberg_Script_JAMROOM extends Netenberg_Script
{
    public function install($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $curl = new Netenberg_cURL;

        $step = 0;

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list(
            $parameters['mysql_hostname'],
            $parameters['mysql_username'],
            $parameters['mysql_password'],
            $parameters['mysql_database']
        ) = $control_panel->insertMysql();

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $operating_system->transpose(
            'http://www.jamroom.net/networkmarket/core_download/jamroom-core-5.2.16.zip',
            array(
                'jamroom-core-5.2.16/*' => sprintf(
                    '%s/%s',
                    $parameters['document_root'],
                    $parameters['directory']
                ),
            )
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/data',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $parse_url = parse_url(sprintf(
            'http://%s/%s', $parameters['domain'], $parameters['directory']
        ));
        $htaccess = sprintf(
            '%s/%s/.htaccess',
            $parameters['document_root'],
            $parameters['directory']
        );
        $contents = file_get_contents($htaccess);
        $contents = preg_replace(
            '#RewriteEngine\s*On#',
            sprintf("RewriteEngine On\nRewriteBase %s", $parse_url['path']),
            $contents
        );
        $contents = preg_replace('#Options#', '# Options', $contents);
        $contents = preg_replace(
            '#AddOutputFilter#', '# AddOutputFilter', $contents
        );
        file_put_contents($htaccess, $contents);

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list($output, $return_var) = $curl->request(
            sprintf(
                'http://%s/%s/install.php?action=install',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'base_url' => sprintf(
                    'http://%s/%s',
                    $parameters['domain'],
                    $parameters['directory']
                ),
                'db_host' => $parameters['mysql_hostname'],
                'db_port' => '3306',
                'db_name' => $parameters['mysql_database'],
                'db_user' => $parameters['mysql_username'],
                'db_pass' => $parameters['mysql_password'],
            ),
            array(),
            array()
        );

        if (strpos($output[1], 'successfully installed!') !== false) {
            log_('DEBUG', sprintf(_('Step %d'), ++$step));
            list($output, $return_var) = $curl->request(
                sprintf(
                    'http://%s/%s/user/signup',
                    $parameters['domain'],
                    $parameters['directory']
                ),
                'GET',
                array(),
                array(),
                array()
            );
            preg_match(
                '#name="jr_html_form_token"\s*value="([^"]*)#',
                $output[1],
                $jr_html_form_token
            );

            log_('DEBUG', sprintf(_('Step %d'), ++$step));
            list($output, $return_var) = $curl->request(
                sprintf(
                    'http://%s/%s/user/signup_save/__ajax=1',
                    $parameters['domain'],
                    $parameters['directory']
                ),
                'POST',
                array(
                    'jr_html_form_token' => $jr_html_form_token[1],
                    'quota_id' => '1',
                    'user_email' => $parameters['user_email'],
                    'user_is_human5734384' => 'on',
                    'user_name' => $parameters['user_name'],
                    'user_passwd1' => $parameters['user_password'],
                    'user_passwd2' => $parameters['user_password'],
                ),
                array(),
                array()
            );
            preg_match('#activate\\\/([^"]*)#', $output[1], $activate);

            log_('DEBUG', sprintf(_('Step %d'), ++$step));
            $curl->request(
                sprintf(
                    'http://%s/%s/user/activate/%s',
                    $parameters['domain'],
                    $parameters['directory'],
                    $activate[1]
                ),
                'GET',
                array(),
                array(),
                array()
            );
            log_('DEBUG', 'Success');

            return parent::install($parameters);
        }
        log_('DEBUG', 'Failure');

        return false;
    }

    public function getCategory()
    {
        return _('Portals/CMS');
    }

    public function getDescription()
    {
        return _('Jamroom is a community content management system. It enables you to bring your entire community online to participate in your websites content creation. If you want to build an online community where everyone has their own identity then Jamroom can provide you with the tools to do it.');
    }

    public function getDetails($parameters)
    {
        $include_php = sprintf(
            '%s/%s/modules/jrCore/include.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($include_php)) {
            return false;
        }
        $contents = file_get_contents($include_php);
        preg_match('#version\'\s*=>\s*\'([\d+\.]+)#', $contents, $version);

        return array(
            'version' => $version[1],
        );
    }

    public function getForm()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $form = new Netenberg_Form();
        $form->addElement('select', 'domain', array(
            'label' => _('Domain'),
            'multiOptions' => $control_panel->getDomains(),
            'required' => true,
        ));
        $form->addElement('text', 'directory', array(
            'description' => _('Leave this field empty if you want to install in the web root for the domain you\'ve selected (i.e., http://domain.com/ ). If you\'d like to install in a subdirectory, please enter the path to the directory relative to the web root for your domain. The final destination subdirectory should not exist, but all others can exist (e.g., http://domain.com/some/sub/directory - In this case, "directory" should not already exist).'),
            'filters' => array(new Netenberg_Filter_Directory()),
            'label' => _('Directory'),
            'validators' => array(new Netenberg_Validate_Directory()),
        ));
        $form->addElement('text', 'user_name', array(
            'label' => _('Username'),
            'required' => true,
        ));
        $form->addElement('text', 'user_password', array(
            'label' => _('Password'),
            'required' => true,
        ));
        $form->addElement('text', 'user_email', array(
            'label' => _('Email'),
            'required' => true,
            'validators' => array(
                array('EmailAddress', false),
            ),
        ));
        $form->addElement('button', 'submit');
        $form->addElement('button', 'reset');
        $form->addDisplayGroup(
            array('domain', 'directory'),
            'location_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => _('Location Details'),
            )
        );
        $form->addDisplayGroup(
            array(
                'user_name',
                'user_password',
                'user_email',
            ),
            'administrator_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => _('Administrator Details'),
            )
        );
        $form->addDisplayGroup(
            array('submit', 'reset'),
            'buttons',
            array(
                'decorators' => $form->getButtonGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
            )
        );

        return $form;
    }

    public function getImage()
    {
        return 'http://www.jamroom.net/skins/jrForest/img/logo.png';
    }

    public function getName()
    {
        return 'JAMROOM';
    }

    public function getItems($parameters)
    {
        return array(
            _('Backend') => array(
                sprintf(
                    '<a href="http://%s/%s/user/login" target="_blank">http://%s/%s/user/login</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['domain'],
                    $parameters['directory']
                ),
                sprintf(_('Username: %s'), $parameters['user_name']),
                sprintf(_('Password: %s'), $parameters['user_password']),
            ),
            _('Frontend') => array(
                sprintf(
                    '<a href="http://%s/%s" target="_blank">http://%s/%s</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['domain'],
                    $parameters['directory']
                ),
            ),
        );
    }

    public function getRequirements()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $apache = $control_panel->getApache();
        $mysql = $control_panel->getMysql();
        $php = $control_panel->getPhp();

        return array(
            'Disk Space' => (
                $control_panel->getSize() >= $this->getSize()
            )? true: false,
            'Apache 2.2+' => (preg_match(
                '#Apache/(2\.[2-9])#', $apache
            ) === 1)? true: false,
            'Apache :: mod_rewrite' => true,
            'MySQL 5.1+' => (preg_match(
                '#Distrib\s*(5\.[1-6])#', $mysql
            ) === 1)? true: false,
            'PHP 5.3+' => (preg_match(
                '#PHP Version\s*=>\s*(5\.[3-9])#', $php
            ) === 1)? true: false,
            'PHP :: mysqli' => (preg_match(
                '#MysqlI Support\s*=>\s*enabled#', $php
            ) === 1)? true: false,
            'PHP :: gd' => (preg_match(
                '#GD Support\s*=>\s*enabled#', $php
            ) === 1)? true: false,
            'PHP :: mbstring' => (preg_match(
                '#Multibyte Support\s*=>\s*enabled#', $php
            ) === 1)? true: false,
            'PHP :: FreeType' => (preg_match(
                '#FreeType\s*Support\s*=>\s*enabled#', $php
            ) === 1)? true: false,
        );
    }

    public function getSize()
    {
        return 29674701;
    }

    public function getSlug()
    {
        return 'jamroom';
    }

    public function getTimestamp()
    {
        return '2014-11-26 21:40:00';
    }

    public function getUrls()
    {
        return array(
            _('Home') => 'http://www.jamroom.net',
            _('Documentation') => 'http://www.jamroom.net/docs',
            _('Support') => 'http://www.jamroom.net/the-jamroom-network/forum',
        );
    }

    public function getVersion()
    {
        return '5.2.16';
    }

    public function uninstall($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $config_php = sprintf(
            '%s/%s/data/config/config.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($config_php)) {
            return false;
        }
        $contents = file_get_contents($config_php);
        preg_match('#jrCore_db_name\']\s*=\s*\'([^\']*)#', $contents, $database);
        preg_match(
            '#jrCore_db_user\']\s*=\s*\'([^\']*)#', $contents, $mysql_username
        );
        $control_panel->deleteMysql($mysql_username[1], $database[1]);

        $operating_system->dispose(sprintf(
            '%s/%s', $parameters['document_root'], $parameters['directory']
        ));

        return parent::uninstall($parameters);
    }
}
