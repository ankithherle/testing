<?php
function isprime($number)
{
    for($k = 2; $k < $number; $k ++) {
        if ($number % $k == 0) {
            break;
        }
    }
    if ($k == $number) {
        return True;
    }
}

$count = 0;
$number = 0;
while (True) {
    if (isprime($number)) {
        $count += 1;
    }
    if ($count == $argv[1]) {
        echo $count . 'th prime number is:' . ' ' . $number . "\n";
        break;
    }
    $number += 1;
}
