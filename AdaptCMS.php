<?php

class Netenberg_Script_AdaptCMS extends Netenberg_Script
{
    public function install($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $curl = new Netenberg_cURL;

        $step = 0;

        $parse_url = parse_url(sprintf(
            'http://%s/%s/', $parameters['domain'], $parameters['directory']
        ));

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list(
            $parameters['mysql_hostname'],
            $parameters['mysql_username'],
            $parameters['mysql_password'],
            $parameters['mysql_database']
        ) = $control_panel->insertMysql();

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $operating_system->transpose(
            'http://kaz.dl.sourceforge.net/project/adaptcms/AdaptCMS%203.x/3.0.x/AdaptCMS_3.0.3.zip',
            array(
                'adaptcms/*' => sprintf(
                    '%s/%s',
                    $parameters['document_root'],
                    $parameters['directory']
                ),
            )
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, false);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/app',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $_htaccess = sprintf(
            '%s/%s/.htaccess',
            $parameters['document_root'],
            $parameters['directory']
        );
        $contents = file_get_contents($_htaccess);
        $contents = preg_replace(
            '#RewriteEngine\s*[oO]n#',
            sprintf("RewriteEngine on\n\tRewriteBase %s", $parse_url['path']),
            $contents
        );
        file_put_contents($_htaccess, $contents);

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $app_htaccess = sprintf(
            '%s/%s/app/.htaccess',
            $parameters['document_root'],
            $parameters['directory']
        );
        $contents = file_get_contents($app_htaccess);
        $contents = preg_replace(
            '#RewriteEngine\s*[oO]n#',
            sprintf("RewriteEngine on\n\tRewriteBase %s", $parse_url['path']),
            $contents
        );
        file_put_contents($app_htaccess, $contents);

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $webroot_htaccess = sprintf(
            '%s/%s/app/webroot/.htaccess',
            $parameters['document_root'],
            $parameters['directory']
        );
        $contents = file_get_contents($webroot_htaccess);
        $contents = preg_replace(
            '#RewriteEngine\s*[oO]n#',
            sprintf("RewriteEngine on\n\tRewriteBase %s", $parse_url['path']),
            $contents
        );
        file_put_contents($webroot_htaccess, $contents);

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/database',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/database',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                '_method' => 'POST',
                'data[database]' =>  $parameters['mysql_database'],
                'data[host]' => $parameters['mysql_hostname'],
                'data[login]' => $parameters['mysql_username'],
                'data[password]' => $parameters['mysql_password'],
                'data[prefix]' => 'adc_',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/sql',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/sql',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                '_method' => 'POST',
                'data[continue]' => '',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/account',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                '_method' => 'POST',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/account',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                '_method' => 'POST',
                'data[Security][1][answer]' => 'cricket',
                'data[Security][1][question]' => 'Your favorite sport?',
                'data[Security][2][answer]' => 'red',
                'data[Security][2][question]' => 'Color of your first car?',
                'data[User][email]' => $parameters['email'],
                'data[User][password]' => $parameters['password'],
                'data[User][password_confirm]' => $parameters['password'],
                'data[User][security_question_hidden]' => 'What was your mothers maiden name?',
                'data[User][username]' => $parameters['username'],
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list($output, $return_var) = $curl->request(
            sprintf(
                'http://%s/%s/install/finish',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );
        if (strpos($output[1], 'Finish Installation') !== false) {
            log_('DEBUG', sprintf(_('Step %d'), ++$step));
            if (!$control_panel->hasSuexec()) {
                $operating_system->chmod(sprintf(
                    '%s/%s/app/Config/config.php',
                    $parameters['document_root'],
                    $parameters['directory']
                ), 644, false);
            }

            log_('DEBUG', sprintf(_('Step %d'), ++$step));
            if (!$control_panel->hasSuexec()) {
                $operating_system->chmod(sprintf(
                    '%s/%s/app/Config/database.php',
                    $parameters['document_root'],
                    $parameters['directory']
                ), 644, false);
            }

            log_('DEBUG', 'Success');

            return parent::install($parameters);
        }
        log_('DEBUG', 'Failure');

        return false;
    }

    public function getCategory()
    {
        return _('Portals/CMS');
    }

    public function getDescription()
    {
        return _('AdaptCMS is a modern Content Management System, adaptable to any type of content website. In existence for now 6 years, AdaptCMS 3.0 uses the latest web technology. jQuery, Bootstrap, API functionality and the cakePHP Framework.');
    }

    public function getDetails($parameters)
    {
        $core_php = sprintf(
            '%s/%s/app/Config/core.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($core_php)) {
            return false;
        }
        $contents = file_get_contents($core_php);
        preg_match('#ADAPTCMS_VERSION\',\s*\'([^\']*)#', $contents, $version);

        return array(
            'version' => $version[1]
        );
    }

    public function getForm()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $form = new Netenberg_Form();
        $form->addElement('select', 'domain', array(
            'label' => _('Domain'),
            'multiOptions' => $control_panel->getDomains(),
            'required' => true,
        ));
        $form->addElement('text', 'directory', array(
            'description' => _('Leave this field empty if you want to install in the web root for the domain you\'ve selected (i.e., http://domain.com/ ). If you\'d like to install in a subdirectory, please enter the path to the directory relative to the web root for your domain. The final destination subdirectory should not exist, but all others can exist (e.g., http://domain.com/some/sub/directory - In this case, "directory" should not already exist).'),
            'filters' => array(new Netenberg_Filter_Directory()),
            'label' => _('Directory'),
            'validators' => array(new Netenberg_Validate_Directory()),
        ));
        $form->addElement('text', 'username', array(
            'label' => _('Username'),
            'required' => true,
        ));
        $form->addElement('text', 'password', array(
            'label' => _('Password'),
            'required' => true,
        ));
        $form->addElement('text', 'email', array(
            'label' => _('Email'),
            'required' => true,
            'validators' => array(
                array('EmailAddress', false),
            ),
        ));
        $form->addElement('button', 'submit');
        $form->addElement('button', 'reset');
        $form->addDisplayGroup(
            array('domain', 'directory'),
            'location_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => 'Location Details',
            )
        );
        $form->addDisplayGroup(
            array('username', 'password', 'email'),
            'administrator_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => _('Administrator Details'),
            )
        );
        $form->addDisplayGroup(
            array('submit', 'reset'),
            'buttons',
            array(
                'decorators' => $form->getButtonGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
            )
        );

        return $form;
    }

    public function getImage()
    {
        return 'http://honourchick.com/image/cms_logos/adapt.jpg';
    }

    public function getName()
    {
        return 'AdaptCMS';
    }

    public function getItems($parameters)
    {
        return array(
            _('Backend') => array(
                sprintf(
                    '<a href="http://%s/%s/login" target="_blank">http://%s/%s/login</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['domain'],
                    $parameters['directory']
                ),
                sprintf(_('Username: %s'), $parameters['username']),
                sprintf(_('Password: %s'), $parameters['password']),
            ),
            _('Frontend') => array(
                sprintf(
                    '<a href="http://%s/%s" target="_blank">http://%s/%s</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['domain'],
                    $parameters['directory']
                ),
            ),
        );
    }

    public function getRequirements()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $apache = $control_panel->getApache();
        $mysql = $control_panel->getMysql();
        $php = $control_panel->getPhp();

        return array(
            'Disk Space' => (
                $control_panel->getSize() >= $this->getSize()
            )? true: false,
            'Apache 2+' => (
                strpos($apache, 'Apache/2') !== false
            )? true: false,
            'MySQL 5.0+' => (
                strpos($mysql, 'Distrib 5') !== false
            )? true: false,
            'PHP 5.2+' => (preg_match(
                '#PHP Version\s*=>\s*(5\.[2-9])#', $php
            ) === 1)? true: false,
        );
    }

    public function getSize()
    {
        return 16882074;
    }

    public function getSlug()
    {
        return 'adapt-cms';
    }

    public function getTimestamp()
    {
        return '2014-03-23 19:28:07';
    }

    public function getUrls()
    {
        return array(
            _('Home') => 'http://www.adaptcms.com',
            _('Documentation') => 'http://documentation.adaptcms.com',
            _('Support') => 'http://www.adaptcms.com/support',
        );
    }

    public function getVersion()
    {
        return '3.0.3';
    }

    public function uninstall($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $config_inc_php = sprintf(
            '%s/%s/app/Config/database.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($config_inc_php)) {
            return false;
        }
        $contents = file_get_contents($config_inc_php);
        preg_match('#database\'\s*=>\s*\'([^\']*)#', $contents, $database);
        preg_match('#login\'\s*=>\s*\'([^\']*)#', $contents, $mysql_username);
        $control_panel->deleteMysql($mysql_username[1], $database[1]);

        $operating_system->dispose(sprintf(
            '%s/%s', $parameters['document_root'], $parameters['directory']
        ));

        return parent::uninstall($parameters);
    }
}
