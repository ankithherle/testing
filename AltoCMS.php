<?php

class Netenberg_Script_ altocms extends Netenberg_Script
{
    public function install($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $curl = new Netenberg_cURL;

        $step = 0;

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list(
            $parameters['mysql_hostname'],
            $parameters['mysql_username'],
            $parameters['mysql_password'],
            $parameters['mysql_database']
        ) = $control_panel->insertMysql();

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $operating_system->transpose(
            'http://softlayer-sng.dl.sourceforge.net/project/altocms/Alto%20CMS%201.0.7/altocms-1.0.7.zip',
            array(
                'altocms-1.0.7/*' => sprintf(
                    '%s/%s',
                    $parameters['document_root'],
                    $parameters['directory']
                ),
            )
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/_tmp',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/app',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/uploads',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $parse_url = parse_url(sprintf(
            'http://%s/%s', $parameters['domain'], $parameters['directory']
        ));
        $htaccess = sprintf(
            '%s/%s/.htaccess',
            $parameters['document_root'],
            $parameters['directory']
        );
        $contents = file_get_contents($htaccess);
        $contents = preg_replace('#Options#', '# Options', $contents);
        $contents = preg_replace(
            '#RewriteEngine [oO]n#',
            sprintf("RewriteEngine on\nRewriteBase %s", $parse_url['path']),
            $contents
        );
        file_put_contents($htaccess, $contents);

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/_run',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'install_env_params' => '1',
                'install_step_next' => 'Next',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/install/',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'install_db_engine' => 'InnoDB',
                'install_db_name' => $parameters['mysql_database'],
                'install_db_params' => '1',
                'install_db_password' => $parameters['mysql_password'],
                'install_db_port' => '3306',
                'install_db_prefix' => 'alto_',
                'install_db_server' => $parameters['mysql_hostname'],
                'install_db_user' => $parameters['mysql_username'],
                'install_step_next' => 'Next',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list($output, $return_var) = $curl->request(
            sprintf(
                'http://%s/%s/install/',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'install_admin_login' => $parameters['admin_login'],
                'install_admin_mail' => $parameters['admin_mail'],
                'install_admin_params' => '1',
                'install_admin_pass' => $parameters['admin_pass'],
                'install_admin_repass' => $parameters['admin_pass'],
                'install_step_next' => 'Next',
            ),
            array(),
            array()
        );
        if (
            strpos($output[1], 'Congratulations! Alto CMS successfully installed') !== false
            or
            strpos($output[1], 'Поздравляем! Alto CMS успешно установлена') !== false
        ) {
            log_('DEBUG', sprintf(_('Step %d'), ++$step));
            $operating_system->rm(sprintf(
                '%s/%s/install',
                $parameters['document_root'],
                $parameters['directory']
            ), true);

            log_('DEBUG', 'Success');

            return parent::install($parameters);
        }
        log_('DEBUG', 'Failure');

        return false;
    }

    public function getCategory()
    {
        return _('Portals/CMS');
    }

    public function getDescription()
    {
        return _('Alto CMS allows you to create a thematic community of people united by common interests, for example, fishing and its own themed social network, such as amateur photographers, fanclub, fans of "Zenith".');
    }

    public function getDetails($parameters)
    {
        $loader_php = sprintf(
            '%s/%s/engine/loader.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($loader_php)) {
            return false;
        }
        $contents = file_get_contents($loader_php);
        preg_match('#ALTO_VERSION\',\s*\'([^\']*)#', $contents, $version);

        return array(
            'version' => $version[1],
        );
    }

    public function getForm()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $form = new Netenberg_Form();
        $form->addElement('select', 'domain', array(
            'label' => _('Domain'),
            'multiOptions' => $control_panel->getDomains(),
            'required' => true,
        ));
        $form->addElement('text', 'directory', array(
            'description' => _('Leave this field empty if you want to install in the web root for the domain you\'ve selected (i.e., http://domain.com/ ). If you\'d like to install in a subdirectory, please enter the path to the directory relative to the web root for your domain. The final destination subdirectory should not exist, but all others can exist (e.g., http://domain.com/some/sub/directory - In this case, "directory" should not already exist).'),
            'filters' => array(new Netenberg_Filter_Directory()),
            'label' => _('Directory'),
            'validators' => array(new Netenberg_Validate_Directory()),
        ));
        $form->addElement('text', 'admin_login', array(
            'label' => _('Username'),
            'required' => true,
        ));
        $form->addElement('text', 'admin_pass', array(
            'label' => _('Password'),
            'required' => true,
        ));
        $form->addElement('text', 'admin_mail', array(
            'label' => _('Email'),
            'required' => true,
            'validators' => array(
                array('EmailAddress', false),
            ),
        ));
        $form->addElement('button', 'submit');
        $form->addElement('button', 'reset');
        $form->addDisplayGroup(
            array('domain', 'directory'),
            'location_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => _('Location Details'),
            )
        );
        $form->addDisplayGroup(
            array('admin_login', 'admin_pass', 'admin_mail',),
            'administrator_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => _('Administrator Details'),
            )
        );
        $form->addDisplayGroup(
            array('submit', 'reset'),
            'buttons',
            array(
                'decorators' => $form->getButtonGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
            )
        );

        return $form;
    }

    public function getImage()
    {
        return 'http://img.obzor.ly/2014-04-14_024903_c6wbzjg2.png';
    }

    public function getName()
    {
        return ' Alto CMS';
    }

    public function getItems($parameters)
    {
        return array(
            _('Backend') => array(
                sprintf(
                    '<a href="http://%s/%s/en" target="_blank">http://%s/%s/en</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['domain'],
                    $parameters['directory']
                ),
                sprintf(_('Username: %s'), $parameters['admin_login']),
                sprintf(_('Password: %s'), $parameters['admin_pass']),
            ),
        );
    }

    public function getRequirements()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $apache = $control_panel->getApache();
        $mysql = $control_panel->getMysql();
        $php = $control_panel->getPhp();

        return array(
            'Disk Space' => (
                $control_panel->getSize() >= $this->getSize()
            )? true: false,
            'Apache 1+' => (
                strpos($apache, 'Apache/1') !== false
                or
                strpos($apache, 'Apache/2') !== false
            )? true: false,
            'Apache :: mod_rewrite' => true,
            'MySQL 4.1+' => (
                strpos($mysql, 'Distrib 4.1') !== false
                or
                strpos($mysql, 'Distrib 5') !== false
            )? true: false,
            'PHP 5.3+' => (preg_match(
                '#PHP Version\s*=>\s*(5\.[3-9])#', $php
            ) === 1)? true: false,
            'PHP :: mbstring' => (preg_match(
                '#Multibyte Support\s*=>\s*enabled#', $php
            ) === 1)? true: false,
            'PHP :: pcre' => (preg_match(
                '#PCRE \(Perl Compatible Regular Expressions\) Support\s*=>\s*enabled#',
                $php
            ) === 1)? true: false,
            'PHP :: SimpleXML' => (preg_match(
                '#Simplexml support\s*=>\s*enabled#', $php
            ) === 1)? true: false,
        );
    }

    public function getSize()
    {
        return 34812723;
    }

    public function getSlug()
    {
        return 'alto-cms';
    }

    public function getTimestamp()
    {
        return '2014-08-28 08:20:02';
    }

    public function getUrls()
    {
        return array(
            _('Home') => 'http://altocms.com',
            _('Community') => 'http://altocms.com/community',
        );
    }

    public function getVersion()
    {
        return '1.0.7';
    }

    public function uninstall($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $config_local_php = sprintf(
            '%s/%s/app/config/config.local.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($config_local_php)) {
            return false;
        }
        $contents = file_get_contents($config_local_php);
        preg_match('#dbname\'\]\s*=\s*\'([^\']*)#', $contents, $database);
        preg_match(
            '#user\'\]\s*=\s*\'([^\']*)#', $contents, $mysql_username
        );
        $control_panel->deleteMysql($mysql_username[1], $database[1]);

        $operating_system->dispose(sprintf(
            '%s/%s', $parameters['document_root'], $parameters['directory']
        ));

        return parent::uninstall($parameters);
    }
}
