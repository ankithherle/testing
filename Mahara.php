<?php

class Netenberg_Script_Mahara extends Netenberg_Script
{
    public function install($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $curl = new Netenberg_cURL;

        $step = 0;

        $config_php = sprintf(
            '%s/%s/config.php',
            $parameters['document_root'],
            $parameters['directory']
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list(
            $parameters['mysql_hostname'],
            $parameters['mysql_username'],
            $parameters['mysql_password'],
            $parameters['mysql_database']
        ) = $control_panel->insertMysql();

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $operating_system->transpose(
            'https://launchpadlibrarian.net/191204037/mahara-1.10.1.tar.bz2',
            array(
                'mahara-1.10.1/htdocs/*' => sprintf(
                    '%s/%s',
                    $parameters['document_root'],
                    $parameters['directory']
                ),
            )
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $operating_system->mkdir(sprintf(
            '%s/%s', $control_panel->getHome(), $parameters['directory']
        ), 777);

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $operating_system->cp(sprintf(
            '%s/%s/config-dist.php',
            $parameters['document_root'],
            $parameters['directory']
        ), $config_php);

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $parse_url = parse_url(sprintf(
            'http://%s/%s', $parameters['domain'], $parameters['directory']
        ));
        $contents = file_get_contents($config_php);
        $contents = preg_replace('#postgres#', 'mysql', $contents);
        $contents = preg_replace(
            '#localhost#', $parameters['mysql_hostname'], $contents
        );
        $contents = preg_replace('#null#', '\'3306\'', $contents);
        $contents = preg_replace(
            '#dbname\s*=\s*\'#',
            sprintf('dbname   = \'%s', $parameters['mysql_database']),
            $contents
        );
        $contents = preg_replace(
            '#dbuser\s*=\s*\'#',
            sprintf('dbuser   = \'%s', $parameters['mysql_username']),
            $contents
        );
        $contents = preg_replace(
            '#dbpass\s*=\s*\'#',
            sprintf('dbpass   = \'%s', $parameters['mysql_password']),
            $contents
        );
        $contents = preg_replace(
            '#\/\/ \$cfg->wwwroot = \'https:\/\/myhost.com\/mahara\/\';#',
            sprintf('$cfg->wwwroot = \'%s/\';', $parse_url['path']),
            $contents
        );
        $contents = preg_replace(
            '#\/path\/to\/uploaddir#',
            sprintf(
                '%s/%s/', $control_panel->getHome(), $parameters['directory']
            ),
            $contents
        );
        file_put_contents($config_php, $contents);

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/index.php',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.php',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=core&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=firstcoredata&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=localpreinst&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=artefact.plans&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=artefact.comment&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=artefact.blog&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=artefact.resume&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=artefact.file&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=artefact.internal&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=auth.none&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=auth.saml&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=auth.ldap&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=auth.internal&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=auth.browserid&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=auth.xmlrpc&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=auth.imap&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=notification.emaildigest&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=notification.email&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=notification.internal&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=search.internal&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=search.elasticsearch&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.resume/entireresume&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.blog/blogpost&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.blog/blog&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.blog/taggedposts&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.file/internalmedia&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.file/html&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.plans/plans&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.internal/contactinfo&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.file/pdf&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.internal/textbox&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.file/gallery&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.file/image&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.file/folder&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.file/filedownload&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.resume/resumefield&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.mygroups&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.mygroups&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.groupmembers&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.wall&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.externalvideo&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.recentforumposts&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.googleapps&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.groupviews&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.groupinfo&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.myviews&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.watchlist&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.inbox&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.myfriends&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.newviews&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.blog/recentposts&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.creativecommons&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.navigation&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.externalfeed&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=blocktype.internal/profileinfo&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=interaction.forum&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=grouptype.course&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=grouptype.standard&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=import.leap&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=import.file&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=export.leap&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=export.html&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=lastcoredata&last=false&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.json.php?name=localpostinst&last=true&sesskey=',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/admin/upgrade.php?finished=1',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list($output, $return_var) = $curl->request(
            sprintf(
                'http://%s/%s/',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'login_password' => 'mahara',
                'login_username' => 'admin',
                'pieform_login' => '',
                'sesskey' => '',
                'submit' => 'Login',
            ),
            array(),
            array()
        );
        preg_match('#sesskey\'\s*:\s*\'([^\']*)#', $output[1], $match);

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'email' => $parameters['email'],
                'password1' => $parameters['password1'],
                'password2' => $parameters['password1'],
                'pieform_requiredfields' => '',
                'sesskey' => $match[1],
                'submit' => 'Processing ...',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $mysqli_connect = mysqli_connect(
            $parameters['mysql_hostname'],
            $parameters['mysql_username'],
            $parameters['mysql_password']
        );
        mysqli_select_db($mysqli_connect, $parameters['mysql_database']);
        mysqli_query($mysqli_connect, sprintf(
            "ALTER TABLE usr
            DEFAULT CHARACTER SET utf8
            COLLATE utf8_general_ci",
            mysqli_real_escape_string($mysqli_connect, $parameters['username'])
        ));
        mysqli_query($mysqli_connect, sprintf(
            "UPDATE usr SET username = '%s' WHERE id = 1",
            mysqli_real_escape_string($mysqli_connect, $parameters['username'])
        ));
        mysqli_close($mysqli_connect);

        log_('DEBUG', 'Success');

        return parent::install($parameters);
    }

    public function getCategory()
    {
        return _('Others');
    }

    public function getDescription()
    {
        return _('A fully featured electronic portfolio, weblog, resume builder and social networking system, connecting users and creating online communities. Mahara is designed to provide users with the tools to create a personal and professional learning and development environment.');
    }

    public function getDetails($parameters)
    {
        return array(
            'version' => $this->getVersion(),
        );
    }

    public function getForm()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $form = new Netenberg_Form();
        $form->addElement('select', 'domain', array(
            'label' => _('Domain'),
            'multiOptions' => $control_panel->getDomains(),
            'required' => true,
        ));
        $form->addElement('text', 'directory', array(
            'description' => _('Leave this field empty if you want to install in the web root for the domain you\'ve selected (i.e., http://domain.com/ ). If you\'d like to install in a subdirectory, please enter the path to the directory relative to the web root for your domain. The final destination subdirectory should not exist, but all others can exist (e.g., http://domain.com/some/sub/directory - In this case, "directory" should not already exist).'),
            'filters' => array(new Netenberg_Filter_Directory()),
            'label' => _('Directory'),
            'validators' => array(new Netenberg_Validate_Directory()),
        ));
        $form->addElement('text', 'username', array(
            'label' => _('Username'),
            'required' => true,
        ));
        $form->addElement('text', 'password1', array(
            'description' => sprintf(_('minimum %d characters'), 6),
            'label' => _('Password'),
            'required' => true,
            'validators' => array(
                array('StringLength', false, array(
                    'min' => 6,
                )),
            )
        ));
        $form->addElement('text', 'email', array(
            'label' => _('Email'),
            'required' => true,
            'validators' => array(
                array('EmailAddress', false),
            ),
        ));
        $form->addElement('button', 'submit');
        $form->addElement('button', 'reset');
        $form->addDisplayGroup(
            array('domain', 'directory'),
            'location_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => _('Location Details'),
            )
        );
        $form->addDisplayGroup(
            array('username', 'password1', 'email'),
            'administrator_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => _('Administrator Details'),
            )
        );
        $form->addDisplayGroup(
            array('submit', 'reset'),
            'buttons',
            array(
                'decorators' => $form->getButtonGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
            )
        );

        return $form;
    }

    public function getImage()
    {
        return 'https://launchpadlibrarian.net/35463410/mahara-64x64.png';
    }

    public function getName()
    {
        return 'Mahara';
    }

    public function getItems($parameters)
    {
        return array(
            _('Backend') => array(
                sprintf(
                    '<a href="http://%s/%s" target="_blank">http://%s/%s</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['domain'],
                    $parameters['directory']
                ),
                sprintf(_('Username: %s'), $parameters['username']),
                sprintf(_('Password: %s'), $parameters['password1']),
            ),
        );
    }

    public function getRequirements()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $apache = $control_panel->getApache();
        $mysql = $control_panel->getMysql();
        $php = $control_panel->getPhp();

        return array(
            'Disk Space' => (
                $control_panel->getSize() >= $this->getSize()
            )? true: false,
            'Apache 1+' => (
                strpos($apache, 'Apache/1') !== false
                or
                strpos($apache, 'Apache/2') !== false
            )? true: false,
            'MySQL 5.0+' => (
                strpos($mysql, 'Distrib 5') !== false
            )? true: false,
            'PHP 5.4+' => (preg_match(
                '#PHP Version\s*=>\s*(5\.[4-9])#', $php
            ) === 1)? true: false,
        );
    }

    public function getSize()
    {
        return 29989274;
    }

    public function getSlug()
    {
        return 'mahara';
    }

    public function getTimestamp()
    {
        return '2014-11-26 02:52:00';
    }

    public function getUrls()
    {
        return array(
            _('Home') => 'https://launchpad.net/mahara',
            _('Documentation') => 'https://wiki.mahara.org/index.php/Mahara_Wiki',
        );
    }

    public function getVersion()
    {
        return '1.10.1';
    }

    public function uninstall($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $config_php = sprintf(
            '%s/%s/config.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($config_php)) {
            return false;
        }
        $contents = file_get_contents($config_php);
        preg_match('#dbname\s*=\s*\'([^\']*)#', $contents, $database);
        preg_match('#dbuser\s*=\s*\'([^\']*)#', $contents, $mysql_username);
        $control_panel->deleteMysql($mysql_username[1], $database[1]);

        $operating_system->dispose(sprintf(
            '%s/%s', $parameters['document_root'], $parameters['directory']
        ));

        $operating_system->dispose(sprintf(
            '%s/%s', $control_panel->getHome(), $parameters['directory']
        ));

        return parent::uninstall($parameters);
    }
}
