<?php

class Netenberg_Script_Symfony extends Netenberg_Script
{
    public function install($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $step = 0;

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list(
            $parameters['mysql_hostname'],
            $parameters['mysql_username'],
            $parameters['mysql_password'],
            $parameters['mysql_database']
        ) = $control_panel->insertMysql();

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $operating_system->transpose(
            'http://get.symfony.com/Symfony_Standard_Vendors_2.7.5.zip',
            array(
                'Symfony/*' => sprintf(
                    '%s/%s',
                    $parameters['document_root'],
                    $parameters['directory']
                ),
            )
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, false);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/app',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $app_dev_php = sprintf(
            '%s/%s/web/app_dev.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        $contents = file_get_contents($app_dev_php);
        $contents = preg_replace('#exit#', '# exit', $contents);
        file_put_contents($app_dev_php, $contents);

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $parameters_yml = sprintf(
            '%s/%s/app/config/parameters.yml',
            $parameters['document_root'],
            $parameters['directory']
        );
        $contents = file_get_contents($parameters_yml);
        $contents = preg_replace(
            '#database_name:\s*symfony#',
            sprintf(
                "database_name: %s",
                $parameters['mysql_database']
            ),
            $contents
        );
        $contents = preg_replace(
            '#database_host:\s*127\.0\.0\.1#',
            sprintf(
                "database_host: %s",
                $parameters['mysql_hostname']
            ),
            $contents
        );
        $contents = preg_replace(
            '#database_port:\s*null#',
            "database_port: 3306",
            $contents
        );
        $contents = preg_replace(
            '#database_user:\s*root#',
            sprintf(
                "database_user: %s",
                $parameters['mysql_username']
            ),
            $contents
        );
        $contents = preg_replace(
            '#database_password:\s*null#',
            sprintf(
                "database_password: %s",
                $parameters['mysql_password']
            ),
            $contents
        );
        $contents = preg_replace(
            '#secret:\s*ThisTokenIsNotSoSecretChangeIt#',
            sprintf(
                "secret: %s",
                sha1('Symfony')
            ),
            $contents
        );
        file_put_contents($parameters_yml, $contents);

        log_('DEBUG', 'Success');

        return parent::install($parameters);
    }

    public function getCategory()
    {
        return _('Others');
    }

    public function getDescription()
    {
        return _('Speed up the creation and maintenance of your PHP web applications. Replace the repetitive coding tasks by power, control and pleasure. ');
    }

    public function getDetails($parameters)
    {
        $bootstrap_php_cache = sprintf(
            '%s/%s/app/bootstrap.php.cache',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($bootstrap_php_cache)) {
            return false;
        }
        $contents = file_get_contents($bootstrap_php_cache);
        preg_match('#const\s*VERSION\s*=\'([^\']*)#', $contents, $version);

        return array(
            'version' => $version[1],
        );
    }

    public function getForm()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $form = new Netenberg_Form();
        $form->addElement('select', 'domain', array(
            'label' => _('Domain'),
            'multiOptions' => $control_panel->getDomains(),
            'required' => true,
        ));
        $form->addElement('text', 'directory', array(
            'description' => _('Leave this field empty if you want to install in the web root for the domain you\'ve selected (i.e., http://domain.com/ ). If you\'d like to install in a subdirectory, please enter the path to the directory relative to the web root for your domain. The final destination subdirectory should not exist, but all others can exist (e.g., http://domain.com/some/sub/directory - In this case, "directory" should not already exist).'),
            'filters' => array(new Netenberg_Filter_Directory()),
            'label' => _('Directory'),
            'validators' => array(new Netenberg_Validate_Directory()),
        ));
        $form->addElement('button', 'submit');
        $form->addElement('button', 'reset');
        $form->addDisplayGroup(
            array('domain', 'directory'),
            'location_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => 'Location Details',
            )
        );
        $form->addDisplayGroup(
            array('submit', 'reset'),
            'buttons',
            array(
                'decorators' => $form->getButtonGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
            )
        );

        return $form;
    }

    public function getImage()
    {
        return 'http://symfony.com/images/common/logo/logo_symfony_header.png';
    }

    public function getName()
    {
        return 'Symfony';
    }

    public function getItems($parameters)
    {
        return array(
            _('Backend') => array(
                sprintf(
                    '<a href="http://%s/%s/web/app_dev.php" target="_blank">http://%s/%s/web/app_dev.php</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['domain'],
                    $parameters['directory']
                ),
            ),
        );
    }

    public function getRequirements()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $apache = $control_panel->getApache();
        $mysql = $control_panel->getMysql();
        $php = $control_panel->getPhp();

        return array(
            'Disk Space' => (
                $control_panel->getSize() >= $this->getSize()
            )? true: false,
            'Apache 1.3+' => (
                strpos($apache, 'Apache/1.3') !== false
                or
                strpos($apache, 'Apache/2') !== false
            )? true: false,
            'MySQL 5.0+/MariaDB 10.0+' => (preg_match(
                '#(Distrib\s*10\.[0-9])(?=.*?MariaDB)|(Distrib\s*(5\.[0-9]))#',
                $mysql
            ) === 1)? true: false,
            'PHP 5.0+' => (preg_match(
                '#PHP Version\s*=>\s*(5\.[0-9])#', $php
            ) === 1)? true: false,
        );
    }

    public function getSize()
    {
        return 38654951;
    }

    public function getSlug()
    {
        return 'symfony';
    }

    public function getTimestamp()
    {
        return '2015-09-25 17:08:00';
    }

    public function getUrls()
    {
        return array(
            _('Home') => 'http://symfony.com',
            _('Documentation') => 'http://symfony.com/doc/current/index.html',
            _('Support') => 'http://symfony.com/community',
        );
    }

    public function getVersion()
    {
        return '2.7.5';
    }

    public function uninstall($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $appDevDebugProjectContainer = sprintf(
            '%s/%s/app/cache/dev/appDevDebugProjectContainer.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($appDevDebugProjectContainer)) {
            return false;
        }
        $contents = file_get_contents($appDevDebugProjectContainer);
        preg_match('#database_name\'\s*=>\s*\'([^\']*)#', $contents, $database);
        preg_match('#database_user\'\s*=>\s*\'([^\']*)#', $contents, $mysql_username);

        $control_panel->deleteMysql($mysql_username[1], $database[1]);

        $operating_system->dispose(sprintf(
            '%s/%s', $parameters['document_root'], $parameters['directory']
        ));

        return parent::uninstall($parameters);
    }
}
