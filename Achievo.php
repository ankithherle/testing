<?php

class Netenberg_Script_Achievo extends Netenberg_Script
{
    public function install($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $curl = new Netenberg_cURL;

        $step = 0;

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list(
            $parameters['mysql_hostname'],
            $parameters['mysql_username'],
            $parameters['mysql_password'],
            $parameters['mysql_database']
        ) = $control_panel->insertMysql();

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $operating_system->transpose(
            'http://www.achievo.org/downloads/achievo/achievo-1.4.5.tar.gz',
            array(
                'achievo-1.4.5/*' => sprintf(
                    '%s/%s',
                    $parameters['document_root'],
                    $parameters['directory']
                ),
            )
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, false);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/achievotmp',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/atk',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/themes',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/modules',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/configs',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/config.inc.php',
                $parameters['document_root'],
                $parameters['directory']
            ), 666, false);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/setup.php',
                $parameters['document_root'],
                $parameters['directory']
            ), 666, false);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $config_inc_php = sprintf(
            '%s/%s/config.inc.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        $contents = file_get_contents($config_inc_php);
        $contents = preg_replace(
            '#host"]\s*=\s*"localhost#',
            sprintf('host"] = "%s', $parameters['mysql_hostname']),
            $contents
        );
        $contents = preg_replace(
            '#achievo_1_4#',
            sprintf('%s', $parameters['mysql_database']),
            $contents
        );
        $contents = preg_replace(
            '#user"]\s*=\s*"demo#',
            sprintf('user"] = "%s', $parameters['mysql_username']),
            $contents
        );
        $contents = preg_replace(
            '#password"]\s*=\s*"demo#',
            sprintf('password"] = "%s', $parameters['mysql_password']),
            $contents
        );
        $contents = preg_replace(
            '#demo#', sprintf('%s', $parameters['password']), $contents
        );
        file_put_contents($config_inc_php, $contents);

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $_htaccess = sprintf(
            '%s/%s/.htaccess',
            $parameters['document_root'],
            $parameters['directory']
        );
        $contents = file_get_contents($_htaccess);
        $contents = preg_replace('#php_#', '# php_', $contents);
        $contents = preg_replace('#SSLO#', '# SSLO', $contents);
        file_put_contents($_htaccess, $contents);

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/index.php',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'auth_pw' => $parameters['password'],
                'auth_user' => 'administrator',
                'login' => 'Login',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/setup.php?atklevel=-1&atkprevlevel=0&',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/setup.php?atklevel=-1&atkprevlevel=0&achievo=8lla9t7lfnu8j16dathr27u930&atkescape=&atkaction=dbcheck',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list($output, $return_var) = $curl->request(
            sprintf(
                'http://%s/%s/setup.php?atklevel=-1&atkprevlevel=0&achievo=8lla9t7lfnu8j16dathr27u930&atkescape=&atkaction=installdb',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );
        if (strpos($output[1], 'Installation completed') !== false) {
            log_('DEBUG', 'Success');

            return parent::install($parameters);
        }
        log_('DEBUG', 'Failure');

        return false;
    }

    public function getCategory()
    {
        return _('Project Management');
    }

    public function getDescription()
    {
        return _('Achievo is a flexible web-based resource management tool for business environments. Achievo\'s resource management capabilities will enable organisations to support their business processes in a simple, but effective manner.');
    }

    public function getDetails($parameters)
    {
        $version_inc = sprintf(
            '%s/%s/version.inc',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($version_inc)) {
            return false;
        }
        $contents = file_get_contents($version_inc);
        preg_match('#achievo_version\s*=\s*"([^"]*)#', $contents, $version);

        return array(
            'version' => $version[1]
        );
    }

    public function getForm()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $form = new Netenberg_Form();
        $form->addElement('select', 'domain', array(
            'label' => _('Domain'),
            'multiOptions' => $control_panel->getDomains(),
            'required' => true,
        ));
        $form->addElement('text', 'directory', array(
            'description' => _('Leave this field empty if you want to install in the web root for the domain you\'ve selected (i.e., http://domain.com/ ). If you\'d like to install in a subdirectory, please enter the path to the directory relative to the web root for your domain. The final destination subdirectory should not exist, but all others can exist (e.g., http://domain.com/some/sub/directory - In this case, "directory" should not already exist).'),
            'filters' => array(new Netenberg_Filter_Directory()),
            'label' => _('Directory'),
            'validators' => array(new Netenberg_Validate_Directory()),
        ));
        $form->addElement('text', 'password', array(
            'label' => _('Password'),
            'required' => true,
        ));
        $form->addElement('button', 'submit');
        $form->addElement('button', 'reset');
        $form->addDisplayGroup(
            array('domain', 'directory'),
            'location_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => 'Location Details',
            )
        );
        $form->addDisplayGroup(
            array('password'),
            'administrator_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => _('Administrator Details'),
            )
        );
        $form->addDisplayGroup(
            array('submit', 'reset'),
            'buttons',
            array(
                'decorators' => $form->getButtonGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
            )
        );

        return $form;
    }

    public function getImage()
    {
        return 'http://www.achievo.org/img/achievo/achievo_logo.png';
    }

    public function getName()
    {
        return 'Achievo';
    }

    public function getItems($parameters)
    {
        return array(
            _('Backend') => array(
                sprintf(
                    '<a href="http://%s/%s" target="_blank">http://%s/%s</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['domain'],
                    $parameters['directory']
                ),
                sprintf(_('Username: administrator')),
                sprintf(_('Password: %s'), $parameters['password']),
            ),
        );
    }

    public function getRequirements()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $apache = $control_panel->getApache();
        $mysql = $control_panel->getMysql();
        $php = $control_panel->getPhp();

        return array(
            'Disk Space' => (
                $control_panel->getSize() >= $this->getSize()
            )? true: false,
            'Apache 2+' => (
                strpos($apache, 'Apache/2') !== false
            )? true: false,
            'MySQL 5.0+' => (
                strpos($mysql, 'Distrib 5') !== false
            )? true: false,
            'PHP 5.2+' => (preg_match(
                '#PHP Version\s*=>\s*(5\.[2-9])#', $php
            ) === 1)? true: false,
            'PHP :: gd' => (preg_match(
                '#GD Support\s*=>\s*enabled#', $php
            ) === 1)? true: false,
        );
    }

    public function getSize()
    {
        return 19398656;
    }

    public function getSlug()
    {
        return 'achievo';
    }

    public function getTimestamp()
    {
        return '2010-09-27 00:00:00';
    }

    public function getUrls()
    {
        return array(
            _('Home') => 'http://www.achievo.org',
            _('Support') => 'http://www.achievo.org/services',
        );
    }

    public function getVersion()
    {
        return '1.4.5';
    }

    public function uninstall($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $config_inc_php = sprintf(
            '%s/%s/config.inc.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($config_inc_php)) {
            return false;
        }
        $contents = file_get_contents($config_inc_php);
        preg_match('#db"]\s*=\s*"([^"]*)#', $contents, $database);
        preg_match('#user"]\s*=\s*"([^"]*)#', $contents, $mysql_username);
        $control_panel->deleteMysql($mysql_username[1], $database[1]);

        $operating_system->dispose(sprintf(
            '%s/%s', $parameters['document_root'], $parameters['directory']
        ));

        return parent::uninstall($parameters);
    }
}
