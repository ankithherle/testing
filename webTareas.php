<?php

class Netenberg_Script_webTareas extends Netenberg_Script
{
    public function install($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $curl = new Netenberg_cURL;

        $step = 0;

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list(
            $parameters['mysql_hostname'],
            $parameters['mysql_username'],
            $parameters['mysql_password'],
            $parameters['mysql_database']
        ) = $control_panel->insertMysql();

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $operating_system->transpose(
            'http://liquidtelecom.dl.sourceforge.net/project/webtareas/1.7/webTareas-v1.7.zip',
            array(
                'webtareas/*' => sprintf(
                    '%s/%s',
                    $parameters['document_root'],
                    $parameters['directory']
                ),
            )
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, false);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/logos_clients',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/files',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/includes/phpmailer',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/includes/jpgraph',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/includes/settings.php',
                $parameters['document_root'],
                $parameters['directory']
            ), 666, false);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/includes/request.class-dbspec.php',
                $parameters['document_root'],
                $parameters['directory']
            ), 666, false);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/includes/library-dbspec.php',
                $parameters['document_root'],
                $parameters['directory']
            ), 666, false);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/installation/setup_db.php',
                $parameters['document_root'],
                $parameters['directory']
            ), 666, false);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/installation/db_var.inc.php',
                $parameters['document_root'],
                $parameters['directory']
            ), 666, false);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/administration/systeminfo.php',
                $parameters['document_root'],
                $parameters['directory']
            ), 666, false);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $arrays_lib_php = sprintf(
            '%s/%s/includes/arrays.lib.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        $contents = file_get_contents($arrays_lib_php);
        $contents = preg_replace(
            '#Options\s*for\s*auto\s*logout#',
            "Options for auto logout\nerror_reporting(0);",
            $contents
        );
        file_put_contents($arrays_lib_php, $contents);

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/installation/setup.php',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/installation/setup.php?#setupAnchor',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'step' => '2',
                'save' => 'I accept the terms in the License Agreement',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/installation/setup.php?#setupAnchor',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'step' => '3',
                'save' => 'Settings',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/installation/setup.php?#setupAnchor',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'updatechecker' => 'false',
                'installationType' => 'offline',
                'dbtype' => 'my',
                'dbserver' => $parameters['mysql_hostname'],
                'dblogin' => $parameters['mysql_username'],
                'dbpasswd' => $parameters['mysql_password'],
                'dbname' => $parameters['mysql_database'],
                'tbprefix' => 'wd_',
                'mkdirMethod' => 'PHP',
                'ftpserver' => '',
                'ftplogin' => '',
                'ftppassword' => '',
                'ftpRoot' => '',
                'notifications' => 'false',
                'notificationMethod' => 'mail',
                'smtpserver' =>  '',
                'smtpport' =>  '',
                'smtpsecure' => '',
                'smtplogin' => '',
                'smtppassword' => '',
                'forcedLogin' => 'false',
                'langDefault' => 'en',
                'defaultCurrency' => 'USD',
                'sitePublish' => 'true',
                'enableTimesheet' => 'true',
                'enableFlextime' => 'false',
                'enableEquipment' => 'false',
                'enableMaterial' => 'false',
                'defaultCostMethod' => '1',
                'autoCreateDir' => 'false',
                'periodUnit' => 'week',
                'weekRule' => '2',
                'firstDayOfWeek' => '1',
                'autoLockPeriod' => '0',
                'root' => sprintf(
                    'http://%s/%s/',
                    $parameters['domain'],
                    $parameters['directory']
                ),
                'loginMethod' => 'PLAIN',
                'adminLogin' => $parameters['adminlogin'],
                'adminPwd' => $parameters['adminpwd'],
                'adminPwa' => $parameters['adminpwd'],
                'adminMail' => $parameters['adminmail'],
                'useLDAP' => 'false',
                'ldapserver' => 'ldap://localhost:389/',
                'ldapversion' => '3',
                'basedn' => 'dc=example,dc=com',
                'ldapuser' => '',
                'ldappassword' => '',
                'contactNameFormat' => 'first_name,\' \',last_name',
                'hourAccuracy' => '1',
                'quantityAccuracy' => ' 0',
                'measureAccuracy' => '2',
                'rateAccuracy' => '2',
                'costAccuracy' => '2',
                'decimalPoint' => '.',
                'thousandsSeparator' => ',',
                'dateFormat' => 'Y-m-d|%Y-%m-%d|Y-m',
                'timeFormat' => '24H',
                'gmtTimezone' => 'true',
                'defaultTimezone' => '0',
                'currencyOnRight' => 'false',
                'googleAds' => 'on',
                'enableTinyMCE' => 'true',
                'lockThreshold' => '0',
                'banThreshold' => '0',
                'detectHijack' => '',
                'step' => '3',
                'action' => 'save',
                'save' => ' Save',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/installation/setup.php?#setupAnchor',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'root' => sprintf(
                    'http://%s/%s',
                    $parameters['domain'],
                    $parameters['directory']
                ),
                'dbtype' => 'my',
                'dbserver' => $parameters['mysql_hostname'],
                'dblogin' => $parameters['mysql_username'],
                'dbpasswd' => $parameters['mysql_password'],
                'dbname' => $parameters['mysql_database'],
                'tbprefix' => 'wd_',
                'adminLogin' => $parameters['adminlogin'],
                'adminPwd' => $parameters['adminpwd'],
                'adminMail' => $parameters['adminmail'],
                'loginMethod' => 'PLAIN',
                'mkdirMethod' => 'PHP',
                'ftpserver' => '',
                'ftplogin' => '',
                'ftppassword' => '',
                'ftpRoot' => '',
                'langDefault' => 'en',
                'enableTimesheet' => 'true',
                'enableFlextime' => 'false',
                'enableEquipment' => 'false',
                'enableMaterial' => 'false',
                'step' => '5',
                'save' => 'Create default folders',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/installation/setup.php?#setupAnchor',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'root' => sprintf(
                    'http://%s/%s',
                    $parameters['domain'],
                    $parameters['directory']
                ),
                'dbtype' => 'my',
                'dbserver' => $parameters['mysql_hostname'],
                'dblogin' => $parameters['mysql_username'],
                'dbpasswd' => $parameters['mysql_password'],
                'dbname' => $parameters['mysql_database'],
                'tbprefix' => 'wd_',
                'adminLogin' => $parameters['adminlogin'],
                'adminPwd' => $parameters['adminpwd'],
                'adminMail' => $parameters['adminmail'],
                'loginMethod' => 'PLAIN',
                'mkdirMethod' => 'PHP',
                'ftpserver' => '',
                'ftplogin' => '',
                'ftppassword' => '',
                'ftpRoot' => '',
                'langDefault' => 'en',
                'enableTimesheet' => 'true',
                'enableFlextime' => 'false',
                'enableEquipment' => 'false',
                'enableMaterial' => 'false',
                'step' => '6',
                'save' => 'Check database',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/installation/setup.php?#setupAnchor',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'root' => sprintf(
                    'http://%s/%s',
                    $parameters['domain'],
                    $parameters['directory']
                ),
                'dbtype' => 'my',
                'dbserver' => $parameters['mysql_hostname'],
                'dblogin' => $parameters['mysql_username'],
                'dbpasswd' => $parameters['mysql_password'],
                'dbname' => $parameters['mysql_database'],
                'tbprefix' => 'wd_',
                'adminLogin' => $parameters['adminlogin'],
                'adminPwd' => $parameters['adminpwd'],
                'adminMail' => $parameters['adminmail'],
                'loginMethod' => 'PLAIN',
                'langDefault' => 'en',
                'enableTimesheet' => 'true',
                'enableFlextime' => 'false',
                'enableEquipment' => 'false',
                'enableMaterial' => 'false',
                'step' => '7',
                'save' => 'Create tables',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list($output, $return_var) = $curl->request(
            sprintf(
                'http://%s/%s/installation/setup.php?#setupAnchor',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'root' => sprintf(
                    'http://%s/%s',
                    $parameters['domain'],
                    $parameters['directory']
                ),
                'step' => '8',
                'save' => 'Finish',
            ),
            array(),
            array()
        );
        if (strpos($output[1], 'Installation Complete') !== false) {
            log_('DEBUG', 'Success');

            return parent::install($parameters);
        }
        log_('DEBUG', 'Failure');

        return false;
    }

    public function getCategory()
    {
        return _('Project Management');
    }

    public function getDescription()
    {
        return _('A WebBased Collaboration OpenSource Tool');
    }

    public function getDetails($parameters)
    {
        $setup_php = sprintf(
            '%s/%s/installation/setup.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($setup_php)) {
            return false;
        }
        $contents = file_get_contents($setup_php);
        preg_match('#version\s*=\s*\'([^\']*)#', $contents, $version);

        return array(
            'version' => $version[1],
        );
    }

    public function getForm()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $form = new Netenberg_Form();
        $form->addElement('select', 'domain', array(
            'label' => _('Domain'),
            'multiOptions' => $control_panel->getDomains(),
            'required' => true,
        ));
        $form->addElement('text', 'directory', array(
            'description' => _('Leave this field empty if you want to install in the web root for the domain you\'ve selected (i.e., http://domain.com/ ). If you\'d like to install in a subdirectory, please enter the path to the directory relative to the web root for your domain. The final destination subdirectory should not exist, but all others can exist (e.g., http://domain.com/some/sub/directory - In this case, "directory" should not already exist).'),
            'filters' => array(new Netenberg_Filter_Directory()),
            'label' => _('Directory'),
            'validators' => array(new Netenberg_Validate_Directory()),
        ));
        $form->addElement('text', 'adminlogin', array(
            'label' => _('Username'),
            'required' => true,
        ));
        $form->addElement('text', 'adminpwd', array(
            'label' => _('Password'),
            'required' => true,
        ));
        $form->addElement('text', 'adminmail', array(
            'label' => _('Email'),
            'required' => true,
            'validators' => array(
                array('EmailAddress', false),
            ),
        ));
        $form->addElement('button', 'submit');
        $form->addElement('button', 'reset');
        $form->addDisplayGroup(
            array('domain', 'directory'),
            'location_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => 'Location Details',
            )
        );
        $form->addDisplayGroup(
            array(
                'adminlogin', 'adminpwd', 'adminmail',
            ),
            'administrator_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => _('Administrator Details'),
            )
        );
        $form->addDisplayGroup(
            array('submit', 'reset'),
            'buttons',
            array(
                'decorators' => $form->getButtonGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
            )
        );

        return $form;
    }

    public function getImage()
    {
        return 'http://c.fsdn.com/allura/p/webtareas/icon';
    }

    public function getName()
    {
        return 'webTareas';
    }

    public function getItems($parameters)
    {
        return array(
            _('Backend') => array(
                sprintf(
                    '<a href="http://%s/%s/general/login.php" target="_blank">http://%s/%s/general/login.php</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['domain'],
                    $parameters['directory']
                ),
                sprintf(_('Username: %s'), $parameters['adminlogin']),
                sprintf(_('Password: %s'), $parameters['adminpwd']),
            ),
        );
    }

    public function getRequirements()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $apache = $control_panel->getApache();
        $mysql = $control_panel->getMysql();
        $php = $control_panel->getPhp();

        return array(
            'Disk Space' => (
                $control_panel->getSize() >= $this->getSize()
            )? true: false,
            'Apache 1+' => (
                strpos($apache, 'Apache/1') !== false
                or
                strpos($apache, 'Apache/2') !== false
            )? true: false,
            'MySQL 5.0+' => (
                strpos($mysql, 'Distrib 5') !== false
            )? true: false,
            'PHP 5.0+' => (preg_match(
                '#PHP Version\s*=>\s*(5\.[0-9])#', $php
            ) === 1)? true: false,
        );
    }

    public function getSize()
    {
        return 34393293;
    }

    public function getSlug()
    {
        return 'webtareas';
    }

    public function getTimestamp()
    {
        return '2014-11-25 08:12:44';
    }

    public function getUrls()
    {
        return array(
            _('Home') => 'http://webtareas.sourceforge.net/general/home.php',
            _('Documentation') => 'http://webtareas.sourceforge.net/general/doc.php',
            _('Support') => 'http://sourceforge.net/p/webtareas/tickets',
        );
    }

    public function getVersion()
    {
        return '1.7';
    }

    public function uninstall($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $settings_php = sprintf(
            '%s/%s/includes/settings.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($settings_php)) {
            return false;
        }
        $contents = file_get_contents($settings_php);
        preg_match(
            '#DBLOGIN\',\'([^\']*)#', $contents, $mysql_username
        );
        preg_match('#DBNAME\',\'([^\']*)#', $contents, $database);
        $control_panel->deleteMysql($mysql_username[1], $database[1]);

        $operating_system->dispose(sprintf(
            '%s/%s', $parameters['document_root'], $parameters['directory']
        ));

        return parent::uninstall($parameters);
    }
}
