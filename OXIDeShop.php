<?php

class Netenberg_Script_OXIDeShop extends Netenberg_Script
{
    public function install($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $curl = new Netenberg_cURL;

        $step = 0;

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list(
            $parameters['mysql_hostname'],
            $parameters['mysql_username'],
            $parameters['mysql_password'],
            $parameters['mysql_database']
        ) = $control_panel->insertMysql();

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $operating_system->transpose(
            'http://support.oxid-esales.com/versions/CE/index.php?php=5.2&target=4.9.2',
            array(
                '*' => sprintf(
                    '%s/%s',
                    $parameters['document_root'],
                    $parameters['directory']
                ),
            )
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, false);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/admin',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/log',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/modules',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/out',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/setup',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/tmp',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/views',
                $parameters['document_root'],
                $parameters['directory']
            ), 777, true);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/config.inc.php',
                $parameters['document_root'],
                $parameters['directory']
            ), 666, false);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/oxid.php',
                $parameters['document_root'],
                $parameters['directory']
            ), 666, false);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        if (!$control_panel->hasSuexec()) {
            $operating_system->chmod(sprintf(
                '%s/%s/oxseo.php',
                $parameters['document_root'],
                $parameters['directory']
            ), 666, false);
        }

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $parse_url = parse_url(sprintf(
            'http://%s/%s', $parameters['domain'], $parameters['directory']
        ));
        $htaccess = sprintf(
            '%s/%s/.htaccess',
            $parameters['document_root'],
            $parameters['directory']
        );
        $contents = file_get_contents($htaccess);
        $contents = preg_replace('#Options#', '# Options', $contents);
        $contents = preg_replace(
            '#RewriteBase \/#',
            sprintf("RewriteBase %s/", $parse_url['path']),
            $contents
        );
        file_put_contents($htaccess, $contents);

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list($output, $return_var) = $curl->request(
            sprintf(
                'http://%s/%s/setup/index.php?redirected=1',
                $parameters['domain'],
                $parameters['directory']
            ),
            'GET',
            array(),
            array(),
            array()
        );
        preg_match('#sid"\s*value="([^"]*)#', $output[1], $match);

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/setup/index.php',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'istep' => '200',
                'sid' => $match[1],
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/setup/index.php',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'check_for_updates' => 'false',
                'check_for_updates' => 'true',
                'country_lang' => '8f241f11095b6bb86.01364904',
                'istep' => '300',
                'location_lang' => 'en',
                'sShopLang' => 'en',
                'sid' => $match[1],
                'use_dynamic_pages' => 'false',
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/setup/index.php',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'iEula' => '1',
                'istep' => '400',
                'sid' => $match[1],
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/setup/index.php',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'aDB[dbHost]' => $parameters['mysql_hostname'],
                'aDB[dbName]' => $parameters['mysql_database'],
                'aDB[dbPwd]' => $parameters['mysql_password'],
                'aDB[dbUser]' => $parameters['mysql_username'],
                'aDB[dbiDemoData]' => '0',
                'aDB[iUtfMode]' => '1',
                'istep' => '410',
                'sid' => $match[1],
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/setup/index.php?istep=420&sid=%s',
                $parameters['domain'],
                $parameters['directory'],
                $match[1]
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/setup/index.php?istep=500&sid=%s',
                $parameters['domain'],
                $parameters['directory'],
                $match[1]
            ),
            'GET',
            array(),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        $curl->request(
            sprintf(
                'http://%s/%s/setup/index.php',
                $parameters['domain'],
                $parameters['directory']
            ),
            'POST',
            array(
                'aAdminData[sLoginName]' => $parameters['email'],
                'aAdminData[sPasswordConfirm]' => $parameters['password'],
                'aAdminData[sPassword]' => $parameters['password'],
                'aPath[sCompileDir]' => sprintf(
                    '%s/%s/tmp/',
                    $parameters['document_root'],
                    $parameters['directory']
                ),
                'aPath[sShopDir]' => sprintf(
                    '%s/%s/',
                    $parameters['document_root'],
                    $parameters['directory']
                ),
                'aPath[sShopURL]' => sprintf(
                    'http://%s/%s/',
                    $parameters['domain'],
                    $parameters['directory']
                ),
                'aSetupConfig[blDelSetupDir]' => '1',
                'istep' => '510',
                'sid' => $match[1],
            ),
            array(),
            array()
        );

        log_('DEBUG', sprintf(_('Step %d'), ++$step));
        list($output, $return_var) = $curl->request(
            sprintf(
                'http://%s/%s/setup/index.php?istep=700&sid=%s',
                $parameters['domain'],
                $parameters['directory'],
                $match[1]
            ),
            'GET',
            array(),
            array(),
            array()
        );
        if (strpos($output[1], 'installed successfully') !== false) {
            log_('DEBUG', sprintf(_('Step %d'), ++$step));
            if (!$control_panel->hasSuexec()) {
                $operating_system->chmod(sprintf(
                    '%s/%s/config.inc.php',
                    $parameters['document_root'],
                    $parameters['directory']
                ), 444, true);
            }

            log_('DEBUG', 'Success');

            return parent::install($parameters);
        }
        log_('DEBUG', 'Failure');

        return false;
    }

    public function getCategory()
    {
        return _('E-Commerce');
    }

    public function getDescription()
    {
        return _('OXID eSales supports you in planning your e-commerce strategy. Using intelligent technical solutions and the huge expertise of our solution partners, we will realize your online shop on all channels. E-commerce becomes affordable and profitable with our efficient operating solutions.');
    }

    public function getDetails($parameters)
    {
        return array(
            'version' => $this->getVersion(),
        );
    }

    public function getForm()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $form = new Netenberg_Form();
        $form->addElement('select', 'domain', array(
            'label' => _('Domain'),
            'multiOptions' => $control_panel->getDomains(),
            'required' => true,
        ));
        $form->addElement('text', 'directory', array(
            'description' => _('Leave this field empty if you want to install in the web root for the domain you\'ve selected (i.e., http://domain.com/ ). If you\'d like to install in a subdirectory, please enter the path to the directory relative to the web root for your domain. The final destination subdirectory should not exist, but all others can exist (e.g., http://domain.com/some/sub/directory - In this case, "directory" should not already exist).'),
            'filters' => array(new Netenberg_Filter_Directory()),
            'label' => _('Directory'),
            'validators' => array(new Netenberg_Validate_Directory()),
        ));
        $form->addElement('text', 'email', array(
            'label' => _('Email'),
            'required' => true,
            'validators' => array(
                array('EmailAddress', false),
            ),
        ));
        $form->addElement('text', 'password', array(
            'description' => sprintf(_('minimum %d characters'), 6),
            'label' => _('Password'),
            'required' => true,
            'validators' => array(
                array('StringLength', false, array(
                    'min' => 6,
                )),
            ),
        ));
        $form->addElement('button', 'submit');
        $form->addElement('button', 'reset');
        $form->addDisplayGroup(
            array('domain', 'directory'),
            'location_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => 'Location Details',
            )
        );
        $form->addDisplayGroup(
            array('email', 'password'),
            'administrator_details',
            array(
                'decorators' => $form->getDefaultGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
                'legend' => _('Administrator Details'),
            )
        );
        $form->addDisplayGroup(
            array('submit', 'reset'),
            'buttons',
            array(
                'decorators' => $form->getButtonGroupDecorator(),
                'disableLoadDefaultDecorators' => true,
            )
        );

        return $form;
    }

    public function getImage()
    {
        return 'http://www.oxid-esales.com/fileadmin/templates/main/res/pics/ag/logoTop.jpg';
    }

    public function getName()
    {
        return 'OXIDeShop';
    }

    public function getItems($parameters)
    {
        return array(
            _('Backend') => array(
                sprintf(
                    '<a href="http://%s/%s/admin" target="_blank">http://%s/%s/admin</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['domain'],
                    $parameters['directory']
                ),
                sprintf(_('Email: %s'), $parameters['email']),
                sprintf(_('Password: %s'), $parameters['password']),
            ),
            _('Frontend') => array(
                sprintf(
                    '<a href="http://%s/%s" target="_blank">http://%s/%s</a>',
                    $parameters['domain'],
                    $parameters['directory'],
                    $parameters['domain'],
                    $parameters['directory']
                ),
            ),
        );
    }

    public function getRequirements()
    {
        $control_panel = Zend_Registry::get('control_panel');
        $apache = $control_panel->getApache();
        $mysql = $control_panel->getMysql();
        $php = $control_panel->getPhp();

        return array(
            'Disk Space' => (
                $control_panel->getSize() >= $this->getSize()
            )? true: false,
            'Apache 2+' => (
                strpos($apache, 'Apache/2') !== false
            )? true: false,
            'MySQL 4.0+' => (
                strpos($mysql, 'Distrib 4') !== false
                or
                strpos($mysql, 'Distrib 5') !== false
            )? true: false,
            'PHP 5.3+' => (preg_match(
                '#PHP Version\s*=>\s*(5\.[3-9])#', $php
            ) === 1)? true: false,
            'PHP :: gd' => (preg_match(
                '#GD Support\s*=>\s*enabled#', $php
            ) === 1)? true: false,
            'PHP :: mysql' => (preg_match(
                '#MySQL Support\s*=>\s*enabled#', $php
            ) === 1)? true: false,
            'PHP :: curl' => (preg_match(
                '#cURL support\s*=>\s*enabled#', $php
            ) === 1)? true: false,
        );
    }

    public function getSize()
    {
        return 82418074;
    }

    public function getSlug()
    {
        return 'oxideshop';
    }

    public function getTimestamp()
    {
        return '2014-11-13 14:08:00';
    }

    public function getUrls()
    {
        return array(
            _('Home') => 'http://www.oxid-esales.com',
            _('Documentation') => 'http://www.oxid-esales.com/de/support-services/dokumentation-und-hilfe/willkommen/ueber-dokumentation-und-hilfe.html',
            _('Suppot') => 'http://www.oxid-esales.com/de/support-services/ueberblick.html',
        );
    }

    public function getVersion()
    {
        return '4.9.2';
    }

    public function uninstall($parameters)
    {
        $control_panel = Zend_Registry::get('control_panel');
        $operating_system = Zend_Registry::get('operating_system');

        $config_inc_php = sprintf(
            '%s/%s/config.inc.php',
            $parameters['document_root'],
            $parameters['directory']
        );
        if (!is_file($config_inc_php)) {
            return false;
        }
        $contents = file_get_contents($config_inc_php);
        preg_match('#dbName\s*=\s*\'([^\']*)#', $contents, $database);
        preg_match('#dbUser\s*=\s*\'([^\']*)#', $contents, $mysql_username);
        $control_panel->deleteMysql($mysql_username[1], $database[1]);

        $operating_system->dispose(sprintf(
            '%s/%s', $parameters['document_root'], $parameters['directory']
        ));

        return parent::uninstall($parameters);
    }
}
